import {AbstractControl, ValidationErrors} from '@angular/forms';

export function matchPassword(matchTo: string): (AbstractControl) => ValidationErrors | null {
  return (control: AbstractControl): ValidationErrors | null => {
    return !!control.parent &&
    !!control.parent.value &&
    control.value === control.parent.controls[matchTo].value ? null : {isMatching: true};
  };
}

export function passwordCorrect(): (AbstractControl) => ValidationErrors | null {
  return (control: AbstractControl): ValidationErrors | null => {
    return !!control.parent && !!control.parent.value &&
    (control.value.toString().match(/[a-z]/) &&
      control.value.toString().match(/[A-Z]/) &&
      control.value.toString().match(/\d/)) ? null : {isCorrect: true};
  };
}
