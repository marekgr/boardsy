import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../services/user.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {User} from '../../../model/user';
import {matchPassword, passwordCorrect} from './PasswordValidator';
import {Subscription} from 'rxjs';
import {MatVerticalStepper} from '@angular/material/stepper';

@Component({
  selector: 'sd-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit, OnDestroy {

  @ViewChild('stepper') stepper: MatVerticalStepper;

  @ViewChild('showModal') showModalButton: ElementRef;

  loading = false;

  userFormGroup: FormGroup;

  private passwordSubscription: Subscription;

  private dataSubscriptions: Subscription[] = [];

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private toastr: ToastrService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.initFormGroups();
  }

  ngOnDestroy(): void {
    this.passwordSubscription.unsubscribe();
    this.dataSubscriptions.forEach(it => it.unsubscribe());
  }

  register() {
    this.loading = true;
    const user: User = new User();
    user.username = this.name.value;
    user.email = this.email.value;
    user.password = this.password.value;

    this.dataSubscriptions.push(this.userService.checkIfUserExists(user.email).subscribe(
      response => {
        if (response) {
          this.showError();
        } else {
          this.dataSubscriptions.push(this.userService.register(user).subscribe(
            result => {
              this.loading = false;
              this.showModalButton.nativeElement.click();
            },
            error => this.toastr.error(error.message, 'Error')));
        }
      },
      error => this.toastr.error(error.message, 'Error')
    ));
  }

  showError() {
    this.loading = false;
    this.stepper.selectedIndex = 1;
    this.toastr.error('Użytkownik z tym adresem email już istnieje');

  }

  accountCreated() {
    this.loading = false;
    this.router.navigate(['']);
  }

  private initFormGroups() {
    this.userFormGroup = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(5)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [
        Validators.required,
        passwordCorrect()]],
      passwordRepeat: ['', [Validators.required, matchPassword('password')]]
    });
    this.passwordSubscription = this.userFormGroup.controls.password.valueChanges.subscribe(() => {
      this.userFormGroup.controls.passwordRepeat.updateValueAndValidity();
    });
  }

  get name() {
    return this.userFormGroup.get('name');
  }

  get email() {
    return this.userFormGroup.get('email');
  }

  get password() {
    return this.userFormGroup.get('password');
  }

  get passwordRepeat() {
    return this.userFormGroup.get('passwordRepeat');
  }
}
