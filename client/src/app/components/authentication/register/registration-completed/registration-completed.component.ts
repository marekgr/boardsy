import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../../../services/user.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-registration-completed',
  templateUrl: './registration-completed.component.html',
  styleUrls: ['./registration-completed.component.less']
})
export class RegistrationCompletedComponent implements OnInit, OnDestroy {

  loading = true;

  private subscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private toastr: ToastrService) {
    this.subscriptions.push(
      this.route.queryParams
        .subscribe(param => {
          this.confirmRegistration(param.token);
        })
    );
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  private confirmRegistration(token: string) {
    this.subscriptions.push(
      this.userService.confirmRegistration(token)
        .subscribe(
          value => this.loading = false,
          error => this.toastr.error('Błąd komunikacji z serwerem')
        ));
  }
}
