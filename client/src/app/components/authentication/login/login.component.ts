import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../../services/authentication/auth-service.service';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../../../services/user.service';
import {User} from "../../../model/user";
import {Router} from "@angular/router";
import {StoreService} from "../../../services/store.service";
import {SessionDataService} from '../../../services/session-data.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})

export class LoginComponent implements OnInit, OnDestroy {

  loading = false;

  credentials = {
    email: '',
    password: '',
  };

  private loggedUser: User;

  private subscriptions: Subscription[] = [];

  constructor(private authService: AuthService,
              private toastr: ToastrService,
              private userService: UserService,
              private store: StoreService,
              private router: Router,
              private sessionService: SessionDataService) {
  }

  ngOnInit() {
    this.ifEmailConfiremed();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(it => it.unsubscribe());
  }

  private ifEmailConfiremed() {

  }

  signIn() {
    this.loading = true;
    this.subscriptions.push(
      this.authService.login(this.credentials).subscribe(data => {
          this.sessionService.saveToken(data.access_token);
          this.subscriptions.push(
            this.userService.getUserData(this.credentials.email, this.credentials.password).subscribe(
              userData => {
                this.loading = false;
                this.sessionService.saveUser(userData);
                this.toastr.success('Zostałes pomyślnie zalogowany');
                this.store.user = this.sessionService.getUser();
                this.router.navigate(['']);
              },
              errorData => {
                this.loading = false;
                this.sessionService.removeToken();
                this.toastr.error('Konto nie zostało aktywowane');
              }
            ));
        },
        error => {
          this.toastr.error(error.error.error_description);
          this.loading = false;
        }));
  }
}
