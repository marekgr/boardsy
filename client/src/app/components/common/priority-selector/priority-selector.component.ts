import {Component, forwardRef, Input} from '@angular/core';
import {PRIORITY_TEXT} from '../priority-control/priority-control.component';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

interface PriorityItem {
  code: string;
  text: string;
}

@Component({
  selector: 'app-priority-selector',
  templateUrl: './priority-selector.component.html',
  styleUrls: ['./priority-selector.component.less'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => PrioritySelectorComponent), multi: true}
  ]
})
export class PrioritySelectorComponent implements ControlValueAccessor {

  priorityList: PriorityItem[] = [];

  @Input()
  priority: string;

  value: string;
  propagateChange: any = () => {
  };

  constructor() {
    Object.keys(PRIORITY_TEXT).forEach(priority =>
      this.priorityList.push({code: priority, text: PRIORITY_TEXT[priority]})
    );
  }

  getPriorityText(code: string) {
    return PRIORITY_TEXT[code];
  }

  selectPriority(priorityCode: string) {
    this.value = priorityCode;
    this.propagateChange(this.value);
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {
  }

  setDisabledState(isDisabled: boolean): void {
  }
}
