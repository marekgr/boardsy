import {Component, forwardRef, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Label} from '../../../model/label';

@Component({
  selector: 'app-color-label-selector',
  templateUrl: './color-label-selector.component.html',
  styleUrls: ['./color-label-selector.component.less'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => ColorLabelSelectorComponent), multi: true}
  ]
})
export class ColorLabelSelectorComponent implements ControlValueAccessor {

  @Input()
  labels: Label[];

  @Input()
  selectedLabel: Label;

  value: Label;

  propagateChange: any = () => {
  };

  constructor() {
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value): void {
    if (value) {
      this.value = value;
    }
  }

  selectLabel(value: Label) {
    this.value = value;
    this.propagateChange(this.value);
  }

  compareFn(l1: Label, l2: Label): boolean {
    return l1 && l2 ? l1.id === l2.id : l1 === l2;
  }
}
