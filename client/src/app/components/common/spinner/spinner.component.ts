import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sd-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.less']
})
export class SpinnerComponent implements OnInit {

  @Input()
  loading = false;

  constructor() {
  }

  ngOnInit(): void {
  }

}
