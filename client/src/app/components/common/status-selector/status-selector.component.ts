import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-status-selector',
  templateUrl: './status-selector.component.html',
  styleUrls: ['./status-selector.component.less'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => StatusSelectorComponent), multi: true}
  ]
})
export class StatusSelectorComponent implements ControlValueAccessor {

  STATUSES = [
    'BACKLOG',
    'IN PROGRESS',
    'CODE REVIEW',
    'TESTING',
    'DONE',
    'TO FIX'
  ];

  value: string;

  propagateChange: any = () => {
  };

  constructor() {
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value): void {
    if (value) {
      this.value = value;
    }
  }

  selectStatus(value: string) {
    this.value = value;
    this.propagateChange(this.value);
  }
}
