import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'prettyDate'
})
export class PrettyDatePipePipe implements PipeTransform {

  transform(date: Date) {
    const dateDiff = (new Date().getTime() - new Date(date).getTime()) / 1000;
    return (dateDiff < 60 ?
      'Mniej niż minutę temu' :
      dateDiff < 3600 ?
        Math.round(dateDiff / 60) + ' min temu' :
        dateDiff / 3600 < 2 ?
          'Godzinę temu' :
          dateDiff / 3600 < 24 ?
            Math.round(dateDiff / 3600) + ' h temu' :
            dateDiff / (3600 * 24) < 1 ?
              'Wczoraj' :
              new Date(date).toLocaleDateString());

  }
}
