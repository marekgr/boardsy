import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FileSystemFileEntry, NgxFileDropEntry} from 'ngx-file-drop';
import {TaskFile} from '../../../model/taskfile';
import {Observable, Subscription} from 'rxjs';
import {FileService} from '../../../services/file.service';
import {StoreService} from '../../../services/store.service';
import {ToastrService} from 'ngx-toastr';
import {Ng2ImgMaxService} from 'ng2-img-max';

@Component({
  selector: 'app-file-manager',
  templateUrl: './file-manager.component.html',
  styleUrls: ['./file-manager.component.less']
})
export class FileManagerComponent implements OnInit, OnDestroy {

  @Input()
  taskId: string;

  @Input()
  taskFileIds: string[] = [];

  @Output()
  taskFileIdChanged: EventEmitter<string[]> = new EventEmitter<string[]>();

  acceptedImageTypes = ['image/gif', 'image/jpeg', 'image/png'];

  taskFiles: TaskFile[] = [];

  uploading = false;

  private subscriptions: Subscription[] = [];

  constructor(private imageService: Ng2ImgMaxService,
              private store: StoreService,
              private fileService: FileService,
              private toastr: ToastrService) {
    console.log('heheh')
  }

  ngOnInit(): void {
    this.getFileThumbnails();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  fileDropped(files: NgxFileDropEntry[]) {
    files.map(
      droppedFile => {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {

          if (this.acceptedImageTypes.includes(file.type)) {
            this.addFileAsImage(file);
          } else {
            this.createTaskFile(file, null);
          }
        });
      }
    );
  }

  resizeImage(file): Observable<File> {
    return this.imageService.resizeImage(file, 185, 130);
  }

  getFilenameExtension(fileName: string) {
    return fileName.substr(fileName.lastIndexOf('.') + 1, fileName.length - 1);
  }

  deleteFile(taskFileID: string) {
    this.subscriptions.push(
      this.fileService.deleteFile(taskFileID).subscribe(
        value => {
          this.taskFiles = this.taskFiles.filter(it => it.id !== taskFileID);
          this.taskFileIds = this.taskFileIds.filter(it => it !== taskFileID);
          this.toastr.success('Plik został usunięty');
        },
        error => this.toastr.error('Błąd podczas usuwania pliku z zadania')
      )
    );
  }

  private addFileAsImage(file: File) {
    this.subscriptions.push(this.resizeImage(file).subscribe(
      blobResult => {

        const reader = new FileReader();
        reader.readAsDataURL(blobResult);
        reader.onloadend = () => {
          this.createTaskFile(file, reader.result.toString());
        };
      }
    ));
  }

  private createTaskFile(file: File, preview: string) {
    this.uploading = true;
    const taskFile = new TaskFile();
    taskFile.name = file.name;
    taskFile.preview = preview;
    taskFile.extension = this.getFilenameExtension(file.name);
    taskFile.createdOn = new Date().toLocaleDateString();
    taskFile.createdBy = this.store.user;

    this.subscriptions.push(this.fileService.addFileToTask(this.taskId, taskFile, file).subscribe(
      result => {
        taskFile.fileID = result;
        this.taskFiles.push(taskFile);
        this.taskFiles.sort();
        this.taskFileIds.push(result);
        this.taskFileIdChanged.emit(this.taskFileIds);
        this.uploading = false;
      },
      error => {
        this.toastr.error('Bląd podczas wysyłania pliku');
        this.uploading = false;
      }
    ));
  }

  private getFileThumbnails() {
    this.taskFileIds.map(id => this.subscriptions.push(this.fileService.getFileThumbnail(id).subscribe(
      result => {
        this.taskFiles.push(result);
        this.taskFiles.sort();
      },
      error => this.toastr.error('Bląd podczas pobierania pliku')
    )));
  }
}
