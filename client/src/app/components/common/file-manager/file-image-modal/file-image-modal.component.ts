import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-file-image-modal',
  templateUrl: './file-image-modal.component.html',
  styleUrls: ['./file-image-modal.component.less']
})
export class FileImageModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: { imageURL: string },
              public dialogRef: MatDialogRef<FileImageModalComponent>) {
  }

  ngOnInit(): void {
  }

  closeModal() {
    this.dialogRef.close();
  }
}
