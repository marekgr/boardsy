import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TaskFile} from '../../../../model/taskfile';
import {StoreService} from '../../../../services/store.service';
import {SessionDataService} from '../../../../services/session-data.service';
import {MatDialog} from '@angular/material/dialog';
import {FileImageModalComponent} from '../file-image-modal/file-image-modal.component';
import {fromEvent} from 'rxjs';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.less']
})
export class FileListComponent implements OnInit {

  FILE_ICONS = {
    DOC: 'fa-file-word',
    DOCX: 'fa-file-word',
    MP4: 'fa-file-video',
    AVI: 'fa-file-video',
    MKV: 'fa-file-video',
    WEBM: 'fa-file-video',
    MOV: 'fa-file-video',
    FLV: 'fa-file-video',
    PPT: 'fa-file-powerpoint',
    PPTX: 'fa-file-powerpoint',
    PDF: 'fa-file-pdf',
    XLS: 'fa-file-excel',
    XLSX: 'fa-file-excel',
    ZIP: 'fa-file-archive',
    RAR: 'fa-file-archive',
    TGZ: 'fa-file-archive'
  };

  @Input()
  files: TaskFile[];

  @Output()
  fileDeleteClicked: EventEmitter<string> = new EventEmitter<string>();

  constructor(private token: SessionDataService,
              private store: StoreService,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  getFileIcon(extension: string) {
    return extension.toUpperCase() in this.FILE_ICONS ? this.FILE_ICONS[extension.toUpperCase()] : 'fa-file-alt';
  }

  openFile(fileID: string) {
    const link = document.createElement('a');
    link.setAttribute('target', '_self');
    link.setAttribute('href', this.store.API_URL + '/tasks/get-file/' + fileID + this.store.accessToken());
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  openImage(fileID: string) {
    const dialogRef = this.dialog.open(FileImageModalComponent, {
      data: {imageURL: this.store.API_URL + '/tasks/get-file/' + fileID + this.store.accessToken()},
      maxWidth: '100vw',
      hasBackdrop: true
    });
  }

  deleteFile(fileID: string) {
    this.fileDeleteClicked.emit(fileID);
  }
}
