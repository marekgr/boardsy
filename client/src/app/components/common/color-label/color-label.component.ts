import {Component, Input, OnInit} from '@angular/core';
import {Label} from '../../../model/label';

@Component({
  selector: 'app-color-label',
  templateUrl: './color-label.component.html',
  styleUrls: ['./color-label.component.less']
})
export class ColorLabelComponent implements OnInit {

  @Input()
  label: Label;

  constructor() {
  }

  ngOnInit(): void {
  }

}
