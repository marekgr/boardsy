import {Component, Input, OnInit} from '@angular/core';

export const PRIORITY_TEXT = {
  LOW: 'Niski priorytet',
  MED: 'Normalny priorytet',
  HIGH: 'Wysoki priorytet',
  IMPROV: 'Usprawnienie',
  FIX: 'Naprawa błędu'
};

@Component({
  selector: 'app-priority-control',
  templateUrl: './priority-control.component.html',
  styleUrls: ['./priority-control.component.less']
})
export class PriorityControlComponent implements OnInit {

  @Input()
  priorityCode: string;

  @Input()
  iconOnly: boolean;

  constructor() {
  }

  ngOnInit(): void {
  }

  getPriorityText(code: string): string {
    return PRIORITY_TEXT[code];
  }
}
