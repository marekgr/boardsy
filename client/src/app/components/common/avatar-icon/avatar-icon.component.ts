import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-avatar-icon',
  templateUrl: './avatar-icon.component.html',
  styleUrls: ['./avatar-icon.component.less']
})
export class AvatarIconComponent implements OnInit {

  @Input()
  customIcon: string;

  @Input()
  userName: string;

  constructor() {
  }

  ngOnInit(): void {
  }

}
