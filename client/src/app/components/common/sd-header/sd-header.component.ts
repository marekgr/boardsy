import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../services/authentication/auth-service.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'sd-header',
  templateUrl: './sd-header.component.html',
  styleUrls: ['./sd-header.component.less']
})
export class SdHeaderComponent implements OnInit {

  constructor(public authService: AuthService,
              private router: Router,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
  }

  logOut() {
    this.authService.logout();
    this.router.navigate(['']);
    this.toastr.success('Zostałes pomyślnie wylogowany');
  }
}
