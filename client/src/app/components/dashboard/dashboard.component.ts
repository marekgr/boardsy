import {Component, OnDestroy, OnInit} from '@angular/core';
import {StoreService} from "../../services/store.service";
import {BoardService} from "../../services/board.service";
import {Board} from "../../model/board";
import {forkJoin, Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TeamService} from "../../services/team.service";
import {Team} from "../../model/team";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit, OnDestroy {

  loaded = false;

  boards: Board[];

  teams: Team[];

  private subscriptions: Subscription;

  constructor(private store: StoreService,
              private boardService: BoardService,
              private teamService: TeamService,
              private toastr: ToastrService) {

    this.getData();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

  private getData() {
    if (!this.store.teams && !this.store.boards) {
      this.subscriptions = forkJoin([
        this.teamService.getTeamsOfUser(this.store.user.id),
        this.boardService.getBoardsOfUser(this.store.user.id)]
      ).subscribe(([teams, boards]) => {
        this.teams = teams;
        this.boards = boards;
        this.store.teams = teams;
        this.store.boards = boards;
        this.loaded = true;
      });
    } else {
      this.teams = this.store.teams;
      this.boards = this.store.boards;
      this.loaded = true;
    }
  }
}
