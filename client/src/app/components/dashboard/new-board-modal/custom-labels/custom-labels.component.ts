import {Component, forwardRef, Input} from '@angular/core';
import {Label} from '../../../../model/label';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {StoreService} from '../../../../services/store.service';

@Component({
  selector: 'app-custom-labels',
  templateUrl: './custom-labels.component.html',
  styleUrls: ['./custom-labels.component.less'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => CustomLabelsComponent), multi: true}
  ]
})
export class CustomLabelsComponent implements ControlValueAccessor {

  @Input()
  labels: Label[] = [];

  @Input()
  canEdit = false;

  @Input()
  canDelete = true;

  labelText = '';

  color = 'green';

  exists = false;

  editing = false;

  currentLabel: Label = null;

  currentlyUsedLabels: Label[] = [];

  propagateChange: any = () => {
  }

  constructor(private store: StoreService) {
    this.getUsedLabels();
  }

  private getUsedLabels() {
    if (this.store.currentBoard !== undefined) {
      this.store.currentBoard.panels.map(
        panel => panel.tasks.map(
          task => task.label ? this.currentlyUsedLabels.push(task.label) : undefined
        )
      );
    }
  }

  createLabel(): void {
    if (this.labelText.length > 0) {
      if (!this.labels.some(label => label.text.toUpperCase() === this.labelText.toUpperCase())) {
        this.labels.push(new Label(this.color, this.labelText));
        this.propagateChange(this.labels);
      } else {
        this.exists = true;
      }
    }
  }

  editLabel(label: Label): void {
    this.editing = true;
    this.labelText = label.text;
    this.color = label.color;
    this.currentLabel = label;
  }

  updateLabel(): void {
    this.labels.find(label => label.id === this.currentLabel.id).color = this.color;
    this.labels.find(label => label.id === this.currentLabel.id).text = this.labelText;
    this.cancelEditing();
    this.propagateChange(this.labels);
  }

  cancelEditing(): void {
    this.editing = false;
    this.labelText = '';
    this.color = 'green';
    this.currentLabel = null;
  }

  removeLabel(label: Label): void {
    this.labels = [...this.labels.filter(item => item.text !== label.text)];
    this.propagateChange(this.labels);
  }

  isLabelInUsage(label: Label): boolean {
    return this.currentlyUsedLabels.some(it => it.id === label.id);
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value): void {
    if (value) {
      this.labels = [...value];
    }
  }

  click(el) {
    console.log(el);
  }
}
