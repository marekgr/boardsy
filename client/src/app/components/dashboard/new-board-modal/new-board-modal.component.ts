import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Board} from '../../../model/board';
import {Team} from '../../../model/team';
import {StoreService} from '../../../services/store.service';
import {User} from '../../../model/user';
import {BoardService} from '../../../services/board.service';
import {Label} from '../../../model/label';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-new-board-modal',
  templateUrl: './new-board-modal.component.html',
  styleUrls: ['./new-board-modal.component.less']
})
export class NewBoardModalComponent implements OnInit, OnDestroy {

  usersAutoComplete = new FormControl();

  boardFormGroup: FormGroup;

  teams: Team[] = [];

  filteredUsers: Observable<User[]>;

  selectedUsers: User[];

  labels: Label[] = [];

  private knownUsers: User[] = [];

  private subscription: Subscription;

  constructor(public dialogRef: MatDialogRef<NewBoardModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { panelID: string },
              private formBuilder: FormBuilder,
              private store: StoreService,
              private boardService: BoardService,
              private toastr: ToastrService) {
    this.teams = store.teams;
  }

  ngOnInit(): void {
    this.initFormGroups();
    this.selectedUsers = [];
    this.getKnownUsersFromTeams();
    this.filteredUsers = this.usersAutoComplete.valueChanges
      .pipe(
        startWith(''),
        map(value => this.filterUsersByUserName(value))
      );
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  removeUser(user: User) {
    this.selectedUsers = this.selectedUsers.filter(item => item.username !== user.username);
    this.knownUsers.push(user);
    this.knownUsers.sort();
    this.usersAutoComplete.setValue('');
  }

  selectTeam() {
    this.selectedUsers = [];
    this.getKnownUsersFromTeams();
  }

  selectUser(user: User) {
    this.selectedUsers.push(user);
    this.knownUsers = this.knownUsers
      .filter(item => item.username !== user.username)
      .sort();
    this.usersAutoComplete.setValue('');
    this.boardFormGroup.controls.team.setValue('');
  }

  createNewBoard() {
    const board: Board = new Board();
    board.name = this.boardFormGroup.controls.name.value;
    board.tag = this.boardFormGroup.controls.tag.value;
    if (this.selectedUsers.length > 0) {
      this.selectedUsers.push(this.store.user);
      board.projectUsers = this.selectedUsers;
    } else {
      const projectTeam = this.boardFormGroup.controls.team.value as Team;
      board.projectUsers = projectTeam.users;
      board.teamID = projectTeam.id;
    }
    board.labels = this.labels;
    this.subscription = this.boardService.create(board).subscribe(
      result => {
        this.toastr.success('Tablica "' + result.name + '" została utworzona');
        this.dialogRef.close({board: result});
      },
      error => this.toastr.error('Błąd podczas zapisu danych')
    );
  }

  private filterUsersByUserName(value: string | User): User[] {
    return (value.hasOwnProperty('username')) ?
      this.knownUsers :
      this.knownUsers.filter(option => option.username.toLowerCase().includes((value as string).toLocaleLowerCase())).sort();
  }

  private initFormGroups() {
    this.boardFormGroup = this.formBuilder.group({
      name: [''],
      tag: [''],
      team: [''],
      usersAutoComplete: this.usersAutoComplete,
    });
  }

  private getKnownUsersFromTeams() {
    this.knownUsers = this.teams
      .map(team => team.users.map(user => user))
      .reduce((prev, next) => prev.concat(next));
  }
}
