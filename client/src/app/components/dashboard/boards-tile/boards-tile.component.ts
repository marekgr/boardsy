import {Component, Input, OnInit} from '@angular/core';
import {BoardService} from '../../../services/board.service';
import {Board} from '../../../model/board';
import {MatDialog} from '@angular/material/dialog';
import {NewBoardModalComponent} from '../new-board-modal/new-board-modal.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-boards-tile',
  templateUrl: './boards-tile.component.html',
  styleUrls: ['./boards-tile.component.less']
})
export class BoardsTileComponent implements OnInit {

  @Input()
  boards: Board[];

  constructor(private boardService: BoardService,
              public dialog: MatDialog,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  getBoardID(board: Board) {
    return board.name;
  }

  createNewBoard() {
    const dialogRef = this.dialog.open(NewBoardModalComponent, {
      width: '600px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.boards.push(result.board);
        this.router.navigate(['/board'], {queryParams: {b: result.board.id, bn: result.board.name}});
      }
    });
  }
}
