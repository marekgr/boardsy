import {Component, Input, OnInit} from '@angular/core';
import {NewTeamModalComponent} from './new-team-modal/new-team-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {Team} from '../../../model/team';
import {BoardService} from '../../../services/board.service';

@Component({
  selector: 'app-teams-tile',
  templateUrl: './teams-tile.component.html',
  styleUrls: ['./teams-tile.component.less']
})
export class TeamsTileComponent implements OnInit {

  @Input()
  teams: Team[];

  constructor(public dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  createNewTeam() {
    const dialogRef = this.dialog.open(NewTeamModalComponent, {
      width: '400px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.teams.push(result.data);
      }
    });
  }

}
