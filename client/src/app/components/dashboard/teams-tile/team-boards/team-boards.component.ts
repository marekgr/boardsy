import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {BoardService} from '../../../../services/board.service';
import {Board} from '../../../../model/board';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-team-boards',
  templateUrl: './team-boards.component.html',
  styleUrls: ['./team-boards.component.less']
})
export class TeamBoardsComponent implements OnInit, OnDestroy {

  @Input()
  teamId: string;

  boards: Board[];

  private subscription: Subscription;

  constructor(private boardService: BoardService,
              private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    this.getBoards(this.teamId);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getBoards(teamID: string) {
    this.subscription = this.boardService.getBoardsOfTeam(teamID)
      .subscribe(value => this.boards = value,
        error => this.toastrService.error('Błąd komunikacji z serwerem'));
  }
}
