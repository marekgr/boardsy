import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {User} from '../../../../model/user';
import {UserService} from '../../../../services/user.service';
import {Observable, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Team} from '../../../../model/team';
import {TeamService} from '../../../../services/team.service';
import {ToastrService} from 'ngx-toastr';
import {StoreService} from '../../../../services/store.service';

@Component({
  selector: 'app-new-team-modal',
  templateUrl: './new-team-modal.component.html',
  styleUrls: ['./new-team-modal.component.less']
})
export class NewTeamModalComponent implements OnInit, OnDestroy {

  users: User[] = [];

  userSelectorControl = new FormControl();

  teamFormGroup: FormGroup;

  filteredUsers: Observable<User[]>;

  private tempEmails: string[] = [];

  private subscriptions: Subscription[] = [];

  constructor(public dialogRef: MatDialogRef<NewTeamModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private userService: UserService,
              private teamService: TeamService,
              private formBuilder: FormBuilder,
              private toastr: ToastrService,
              public store: StoreService) {
  }

  ngOnInit(): void {
    this.users.push(this.store.user);
    this.teamFormGroup = this.formBuilder.group({
      name: [''],
      usersAutoComplete: this.userSelectorControl,
    });
    this.subscriptions.push(this.userSelectorControl.valueChanges.subscribe(
      value => this.searchUserByQuery(value)
    ));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  searchUserByQuery(query: string) {
    if (query.length < 2) {
      this.filteredUsers = null;
      return;
    }
    this.filteredUsers = this.userService.search(query).pipe(
      map(value => value)
    );
  }

  createTeam() {
    const team: Team = new Team();
    team.name = this.teamFormGroup.controls.name.value;
    team.users = this.users.filter(item => item.id !== null);
    team.tempEmails = this.tempEmails;
    this.subscriptions.push(
      this.teamService.save(team).subscribe(
        value =>{
          this.toastr.success('Stworzono nowy zespół');
          this.dialogRef.close({data: value});
        },
        error => this.toastr.error('Błąd podczas zapisu danych')
      )
    );
  }

  addEmailUser(email: string) {
    if (!this.users.some(item => item.email === email)) {
      const user: User = new User();
      user.email = email;
      this.users.push(user);
      this.tempEmails.push(email);
    }
    this.userSelectorControl.setValue('');
  }

  addUser(user: User) {
    if (!this.users.some(item => item.id === user.id)) {
      this.users.push(user);
    }
    this.userSelectorControl.setValue('');
  }

  emailValid(): boolean {
    const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(this.userSelectorControl.value.toString());
  }
}
