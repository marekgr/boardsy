import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../services/authentication/auth-service.service";
import {SessionDataService} from '../services/session-data.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  title = 'client';

  loaded = false;

  private subscription: Subscription;

  constructor(private router: Router,
              private authService: AuthService,
              private sessionService: SessionDataService) {
  }

  routeActive(route: string) {
    return this.router.isActive(route, true);
  }

  refreshLogin() {
    this.subscription = this.authService.checkLogin().subscribe(
      result => {
      },
      error => {
        this.sessionService.removeToken();
        this.sessionService.removeUser();
        this.router.navigate(['']);
      }
    );
  }
}
