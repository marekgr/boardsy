import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Panel} from '../../../model/panel';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {Task} from '../../../model/task';
import {NewTaskModalComponent} from './task/new-task-modal/new-task-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmModalComponent} from '../../common/confirm-modal/confirm-modal.component';
import {PanelService} from '../../../services/panel.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.less']
})
export class PanelComponent implements OnInit, OnDestroy {

  @Input()
  panel: Panel;

  @Output()
  panelRemoved: EventEmitter<string> = new EventEmitter();

  private subscriptions: Subscription[] = [];

  constructor(public dialog: MatDialog,
              private panelService: PanelService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(it => it.unsubscribe());
  }

  drop(event: CdkDragDrop<Task[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      if (event.previousIndex !== event.currentIndex) {
        this.subscriptions.push(
          this.panelService.updateTasksOrder(this.panel.id, this.panel.tasks.map(task => task.id)).subscribe(
            result => console.log('Transfer in panel')
          )
        );
      }
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      const previousPanelID = event.previousContainer.element.nativeElement.getAttribute('panelID');
      const currentPanelID = event.container.element.nativeElement.getAttribute('panelID');
      this.subscriptions.push(
        this.panelService.updateTasksBetweenPanels(
          previousPanelID,
          currentPanelID,
          event.previousContainer.data.map(task => task.id),
          event.container.data.map(task => task.id)).subscribe(
          next => console.log('Transfer between panels')
        )
      );
    }
  }

  createTask(): void {
    const dialogRef = this.dialog.open(NewTaskModalComponent, {
      width: '900px',
      data: {panelID: this.panel.id}
    });

    this.subscriptions.push(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.panel.tasks.push(result.task);
        }
      })
    );
  }

  removeTask(task: Task): void {
    this.panel.tasks = this.panel.tasks.filter(item => item.id !== task.id);
  }

  confirmDeleteModal() {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      maxWidth: '400px',
      data: {modalContent: 'Usunąć ten panel?'}
    });
    this.subscriptions.push(
      dialogRef.afterClosed().subscribe(
        result => result ? this.removePanel() : null
      )
    );
  }

  removePanel(): void {
    this.panelRemoved.emit(this.panel.id);
  }
}
