import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Task} from '../../../../../model/task';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {StoreService} from '../../../../../services/store.service';
import {TaskService} from '../../../../../services/task.service';
import {Subscription} from 'rxjs';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-new-task-modal',
  templateUrl: './new-task-modal.component.html',
  styleUrls: ['./new-task-modal.component.less']
})
export class NewTaskModalComponent implements OnInit, OnDestroy {

  task: Task = new Task();

  private subscription: Subscription;

  constructor(public dialogRef: MatDialogRef<NewTaskModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { panelID: string },
              public store: StoreService,
              private taskPanelService: TaskService,
              private toastr: ToastrService) {

  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onSubmit() {
    this.task.createdByUser = this.store.user;
    this.subscription = this.taskPanelService
      .createTask(this.task, this.data.panelID).subscribe(
        result => this.dialogRef.close({task: result}),
        error => this.toastr.error('Błąd podczas zapisu danych')
      );
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }
}
