import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Task} from '../../../../../model/task';
import {StoreService} from '../../../../../services/store.service';
import {FormControl} from '@angular/forms';
import {Observable, Subscription} from 'rxjs';
import {User} from '../../../../../model/user';
import {map, startWith} from 'rxjs/operators';
import {TaskService} from '../../../../../services/task.service';
import {ToastrService} from 'ngx-toastr';
import {Label} from '../../../../../model/label';

@Component({
  selector: 'app-taskview-modal',
  templateUrl: './task-view-modal.component.html',
  styleUrls: ['./task-view-modal.component.less']
})


export class TaskViewModalComponent implements OnInit, OnDestroy {

  task: Task;

  usersAutoComplete = new FormControl();

  filteredUsers: Observable<User[]>;

  file;

  private subscriptions: Subscription[] = [];

  constructor(
    public dialogRef: MatDialogRef<TaskViewModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { task: Task, panelName: string },
    public store: StoreService,
    private taskService: TaskService,
    private toastr: ToastrService) {
    this.task = JSON.parse(JSON.stringify(this.data.task));
  }


  ngOnInit(): void {
    this.filteredUsers = this.usersAutoComplete.valueChanges
      .pipe(
        startWith(''),
        map(value => this.filterUsersByUserName(value))
      );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(it => it.unsubscribe());
  }

  updateTitle(title: string) {
    this.subscriptions.push(
      this.taskService.updateTaskTitle(this.task.id, title)
        .subscribe(
          result => this.data.task.title = title
        )
    );
  }

  updatePriority(priority: string) {
    this.subscriptions.push(
      this.taskService.updateTaskPriority(this.task.id, priority)
        .subscribe(
          result => this.data.task.priority = priority
        )
    );
  }

  updateLabel(label: Label) {
    this.subscriptions.push(
      this.taskService.updateTaskLabel(this.task.id, label)
        .subscribe(
          result => this.data.task.label = label
        )
    );
  }

  updateStatus(status: string) {
    this.subscriptions.push(
      this.taskService.updateTaskStatus(this.task.id, status)
        .subscribe(
          result => this.data.task.status = status
        )
    );
  }

  updateAssignedUser(user: User) {
    this.subscriptions.push(
      this.taskService.updateTaskAssignedUser(this.task.id, user)
        .subscribe(
          result => this.data.task.assignedUser = user
        )
    );
  }

  updateFiles(fileIds: string[]) {
    this.data.task.fileIDs = fileIds;
  }

  displayFn(user: User): string {
    return user ? user.username : null;
  }

  private filterUsersByUserName(value: string | User): User[] {
    return (value.hasOwnProperty('username')) ?
      this.store.currentBoard.projectUsers :
      this.store.currentBoard.projectUsers.filter(option => option.username
        .toLowerCase()
        .includes((value as string)
          .toLocaleLowerCase()))
        .sort();
  }

  showSection(section: HTMLElement) {
    section.hidden = false;
  }

  hideSection(section: HTMLElement) {
    section.hidden = true;
  }

  sectionHidden(section: HTMLElement) {
    return section.hidden;
  }
}
