import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Comment} from '../../../../../../model/comment';
import {TaskService} from '../../../../../../services/task.service';
import {StoreService} from '../../../../../../services/store.service';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmModalComponent} from '../../../../../common/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.less']
})
export class CommentListComponent implements OnDestroy, OnInit {

  @Input()
  commentList: Comment[];

  @Input()
  taskID: string;

  @Output()
  commentsChanged: EventEmitter<Comment[]> = new EventEmitter<Comment[]>();

  comment: Comment = new Comment();

  comments: Comment[];

  subscriptions: Subscription[] = [];

  date = new Date();

  constructor(private taskService: TaskService,
              public store: StoreService,
              private toastr: ToastrService,
              private dialog: MatDialog) {

  }

  addComment() {
    if (!this.comments) {
      this.comments = [];
    }
    this.comment.commentedOn = new Date();
    this.comment.commentedBy = this.store.user;
    this.subscriptions.push(
      this.taskService.addComment(this.taskID, this.comment).subscribe(
        result => {
          this.commentsChanged.emit(result.comments);
          this.comments = result.comments;
          this.comment = new Comment();
        },
        error => this.toastr.error('Błąd podczas zapisu danych')
      )
    );
  }

  deleteComment(comment: Comment) {
    this.subscriptions.push(
      this.taskService.deleteComment(comment.id).subscribe(
        result => {
          this.comments = this.comments.filter(item => item.id !== comment.id);
          this.commentsChanged.emit(this.comments);
          this.comment = new Comment();
        },
        error => this.toastr.error('Błąd podczas zapisu danych')
      )
    );
  }

  editComment(comment: Comment) {
    this.subscriptions.push(
      this.taskService.updateComment(comment).subscribe(
        result => {
          this.commentsChanged.emit(this.comments);
        },
        error => this.toastr.error('Błąd podczas zapisu danych')
      )
    );
  }

  confirmDeleteModal(comment: Comment) {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      maxWidth: '400px',
      data: {modalContent: 'Usunąć komentarz?'}
    });
    this.subscriptions.push(
      dialogRef.afterClosed().subscribe(
        result => result ? this.deleteComment(comment) : null
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(it => it.unsubscribe());
  }

  ngOnInit(): void {
    this.comments = JSON.parse(JSON.stringify(this.commentList));
  }
}
