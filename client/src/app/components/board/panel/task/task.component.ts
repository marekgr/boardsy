import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Task} from '../../../../model/task';
import {MatDialog} from '@angular/material/dialog';
import {TaskViewModalComponent} from './taskview-modal/task-view-modal.component';
import {ConfirmModalComponent} from '../../../common/confirm-modal/confirm-modal.component';
import {TaskService} from '../../../../services/task.service';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';

export const PRIORITY_TEXT = {
  LOW: 'Niski priorytet',
  MED: 'Normalny priorytet',
  HIHG: 'Wysoki priorytet',
  IMPROV: 'Usprawnienie',
  FIX: 'Naprawa błędu'
};

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.less']
})

export class TaskComponent implements OnInit, OnDestroy {

  @Input()
  task: Task;

  @Input()
  panelName: string;

  @Output()
  taskDeleted: EventEmitter<Task> = new EventEmitter<Task>();

  private subscriptions: Subscription[] = [];

  constructor(public dialog: MatDialog,
              private taskService: TaskService,
              private toastr: ToastrService) {
  }

  openTask(): void {
    this.subscriptions.push(
      this.taskService.refreshTask(this.task.id).subscribe(
        data => {
          Object.assign(this.task, data);
          const dialogRef = this.dialog.open(TaskViewModalComponent, {
            width: '900px',
            data: {task: this.task, panelName: this.panelName}
          });
        },
        error1 => this.toastr.error('Błąd podczas aktualizacji danych')
      )
    );
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(it => it.unsubscribe());
  }

  confirmDeletion() {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      maxWidth: '400px',
      data: {modalContent: 'Usunąć to zadanie?'}
    });
    this.subscriptions.push(
      dialogRef.afterClosed().subscribe(
        result => result ? this.deleteTask() : null
      )
    );
  }

  deleteTask() {
    this.subscriptions.push(
      this.taskService.daleteTask(this.task.id).subscribe(
        result => this.taskDeleted.emit(this.task)
      )
    );
  }
}
