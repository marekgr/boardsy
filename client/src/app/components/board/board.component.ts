import {Component, OnDestroy, OnInit} from '@angular/core';
import {Board} from '../../model/board';
import {ActivatedRoute, Router} from '@angular/router';
import {BoardService} from '../../services/board.service';
import {forkJoin, Subscription} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {Panel} from '../../model/panel';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {StoreService} from '../../services/store.service';
import {PanelService} from '../../services/panel.service';
import {Privileges} from '../../model/privileges';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.less']
})
export class BoardComponent implements OnInit, OnDestroy {

  panel: Panel = new Panel();

  board: Board;

  boardID: string;

  subscriptions: Subscription[] = [];

  private interval;

  private checkUpdateSubscription: Subscription;

  constructor(private router: Router,
              private boardService: BoardService,
              private toastr: ToastrService,
              private panelService: PanelService,
              private route: ActivatedRoute,
              public store: StoreService) {
    this.subscriptions.push(
      this.route.queryParams
        .subscribe(params => {
          this.boardID = params.b;
          this.getBoardData(params.b);
        })
    );
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
    if (this.checkUpdateSubscription) {
      this.checkUpdateSubscription.unsubscribe();
    }
    clearInterval(this.interval);
    this.store.privileges = null;
  }

  createPanel() {
    if (this.panel.name && this.panel.name.length > 0) {
      this.subscriptions.push(
        this.panelService.createPanel(this.panel, this.boardID).subscribe(
          result => this.board.panels.push(result)
        )
      );
      this.panel.name = '';
    }
  }

  removePanel(panelID: string) {
    this.subscriptions.push(
      this.panelService.deletePanel(panelID).subscribe(
        result => {
          this.board.panels = this.board.panels.filter(panel => panel.id !== panelID);
        }
      )
    );
  }

  drop(event: CdkDragDrop<Panel[]>) {
    moveItemInArray(this.board.panels, event.previousIndex, event.currentIndex);
    if (event.previousIndex !== event.currentIndex) {
      this.subscriptions.push(
        this.boardService.updatePanelsOrder(this.board.id, this.board.panels.map(panel => panel.id))
          .subscribe(
            result => console.log('Transfer in board')
          )
      );
    }
  }

  getBoardData(id: string) {
    this.subscriptions.push(forkJoin([
      this.boardService.getBoard(id),
      this.boardService.getUserPrivileges(id)]
    ).subscribe(([board, privileges]) => {
      this.store.currentBoard = board;
      this.board = this.store.currentBoard;
      this.store.privileges = privileges as Privileges;
      this.checkForUpdatesStart();
    }, error => {
      this.toastr.error('Nie jesteś członkiem tego projektu lub nie masz wystarczających uprawnień');
    }));
  }

  private checkForUpdatesStart() {
    this.interval = setInterval(() => {
      this.checkUpdateSubscription = this.boardService.checkForBoardUpdates(this.boardID, this.board.version)
        .subscribe(
          next => {
            if (next === true) {
              clearInterval(this.interval);
              this.checkUpdateSubscription.unsubscribe();
              this.getBoardData(this.boardID);
            }
          },
          error => console.log(error)
        );
    }, 3000);
  }
}
