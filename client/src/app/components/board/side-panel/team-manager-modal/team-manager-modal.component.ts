import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {StoreService} from '../../../../services/store.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {User} from '../../../../model/user';
import {UserPrivileges} from '../../../../services/board.service';

interface ProjectMember {
  user: User;
  isAdmin: boolean;
}

@Component({
  selector: 'app-team-manager-modal',
  templateUrl: './team-manager-modal.component.html',
  styleUrls: ['./team-manager-modal.component.less']
})
export class TeamManagerModalComponent implements OnInit {

  displayedColumns: string[] = ['username', 'email', 'isAdmin', 'canCreateTask', 'canEditTask', 'canDeleteTask'];
  dataSource;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(public dialogRef: MatDialogRef<TeamManagerModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { usersData: UserPrivileges[] },
              public store: StoreService) {
  }

  ngOnInit(): void {
    this.setupDataSource();
  }

  private setupDataSource() {
    this.dataSource = new MatTableDataSource(this.data.usersData);
    this.dataSource.filterPredicate = (data: UserPrivileges, filter: string) =>
      (data.user.username + data.user.email)
        .toLocaleLowerCase()
        .indexOf(filter.toLowerCase()) !== -1;
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'email':
          return item.user.email;
        case 'username':
          return item.user.username;
        case 'isAdmin':
          return item.privileges.canManageBoard;
        case 'canCreateTask':
          return item.privileges.canCreateTask;
        case 'canEditTask':
          return item.privileges.canEditTask;
        case 'canDeleteTask':
          return item.privileges.canDeleteTaska;
        default:
          return item[property];
      }
    };
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  updateBoardAdmins() {
    this.dialogRef.close({userPrivileges: this.dataSource.data});
  }
}
