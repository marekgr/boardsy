import {Component, Input, OnDestroy} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {LabelsManageModalComponent} from './labels-manage-modal/labels-manage-modal.component';
import {TeamManagerModalComponent} from './team-manager-modal/team-manager-modal.component';
import {Board} from '../../../model/board';
import {BoardService, UserPrivileges} from '../../../services/board.service';
import {StoreService} from '../../../services/store.service';
import {ToastrService} from 'ngx-toastr';
import {Label} from '../../../model/label';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-side-panel',
  templateUrl: './side-panel.component.html',
  styleUrls: ['./side-panel.component.less']
})
export class SidePanelComponent implements OnDestroy {

  visible = false;

  @Input()
  board: Board;

  private subscriptions: Subscription[] = [];

  constructor(public dialog: MatDialog,
              private boardService: BoardService,
              private store: StoreService,
              private toastr: ToastrService) {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(it => it.unsubscribe());
  }

  openLabelManager(): void {
    const dialogRef = this.dialog.open(LabelsManageModalComponent, {
      width: '400px',
      data: {labels: this.board.labels}
    });

    this.subscriptions.push(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.updateBoardLabels(result.labels);
        }
      })
    );
  }

  openTeamManager(): void {
    this.subscriptions.push(
      this.boardService.getBoardUsersPrivileges(this.board.id).subscribe(
        usersPrivileges => {
          const dialogRef = this.dialog.open(TeamManagerModalComponent, {
            width: '700px',
            data: {usersData: usersPrivileges},
          });

          this.subscriptions.push(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                this.updateUsersPrivileges(result.userPrivileges);
              }
            })
          );
        }
      )
    );
  }

  switchVisibility() {
    this.visible = !this.visible;
  }

  updateUsersPrivileges(userPrivileges: UserPrivileges[]) {
    this.subscriptions.push(
      this.boardService.updateBoardUsersPrivileges(this.board.id, userPrivileges).subscribe(
        next => this.toastr.success('Dane aktualizowane pomyślnie'),
        error => this.toastr.error('Błąd podczas aktualizacji danych')
      )
    );
  }

  updateBoardLabels(labels: Label[]) {
    this.subscriptions.push(
      this.boardService.updateBoardLabels(this.board.id, labels).subscribe(
        next => {
          this.toastr.success('Dane aktualizowane pomyślnie');
          this.reloadBoard();
        },
        error => this.toastr.error('Błąd podczas aktualizacji danych')
      )
    );
  }

  reloadBoard() {
    this.subscriptions.push(
      this.boardService.getBoard(this.board.id).subscribe(
        next => Object.assign(this.store.currentBoard, next),
        error => this.toastr.error('Błąd podczas aktualizacji danych')
      )
    );
  }
}
