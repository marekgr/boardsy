import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Label} from '../../../../model/label';

@Component({
  selector: 'app-labels-manage-modal',
  templateUrl: './labels-manage-modal.component.html',
  styleUrls: ['./labels-manage-modal.component.less']
})
export class LabelsManageModalComponent implements OnInit {

  labels: Label[];

  constructor(public dialogRef: MatDialogRef<LabelsManageModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { labels: Label[] }) {
    this.labels = [...data.labels];
  }

  ngOnInit(): void {
  }

  updateLabels() {
    this.dialogRef.close({labels: this.labels});
  }
}
