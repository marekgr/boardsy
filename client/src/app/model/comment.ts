import { User } from './user';

export class Comment {
  id: string;
  content: string;
  commentedBy: User;
  commentedOn: Date;

  constructor() {
    this.content = '';
  }
}
