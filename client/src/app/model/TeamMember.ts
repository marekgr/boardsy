export class TeamMember {
  id: string;
  username: string;
  email: string;
}
