import {User} from './user';

export class TaskFile {
  id: string;
  name: string;
  extension: string;
  createdBy: User;
  createdOn: string;
  fileID: string;
  preview?: string;
}
