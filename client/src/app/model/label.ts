export class Label {
  id: string;
  color: string;
  text: string;

  constructor(color: string, text: string) {
    this.color = color;
    this.text = text;
  }
}
