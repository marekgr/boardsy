import {Label} from './label';
import {Panel} from './panel';
import {User} from './user';

export class Board {
  id: string;
  name: string;
  tag: string;
  createdOnDate: string;
  labels: Label[];
  panels: Panel[];
  version: number;
  projectUsers: User[];
  teamID?: string;

  constructor() {
    this.createdOnDate = new Date().toUTCString();
  }
}
