export class User {
  id: string;
  username: string;
  email: string;
  password?: string;
  active?: boolean;
  isAdmin?: boolean;

  constructor(
    id: string = null,
    username: string = null,
    email: string = null,
    password: string = null,
    active: boolean = false) {
    this.id = id;
    this.username = username;
    this.email = email;
    this.password = password;
    this.active = active;
  }
}
