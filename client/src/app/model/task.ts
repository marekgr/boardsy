import {User} from './user';
import {Comment} from './comment';
import {Label} from './label';

export class Task {
  id: string;
  title: string;
  description: string;
  tag: string;
  label: Label;
  status: string;
  priority: string;
  comments: Comment[];
  assignedUser: User;
  createdByUser: User;
  createdOn: Date;
  fileIDs: string[];

  constructor() {
    this.title = '';
    this.description = '';
    this.tag = '';
    this.label = null;
    this.status = 'BACKLOG';
    this.priority = 'LOW';
    this.comments = [];
    this.assignedUser = null;
    this.createdByUser = null;
    this.createdOn = new Date();
    this.fileIDs = [];
  }
}
