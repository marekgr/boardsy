export class Privileges {
  canEditTask: boolean;
  canCreateTask: boolean;
  canChangeOrder: boolean;
  canDeleteTask: boolean;
  canDeletePanel: boolean;
  canManageBoard: boolean;
}
