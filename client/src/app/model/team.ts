import {User} from "./user";

export class Team {
  id: string;
  name: string;
  users: User[];
  tempEmails: string[];
}
