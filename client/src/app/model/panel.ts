import {Task} from './task';

export class Panel {
  id: string;
  name: string;
  tasks: Task[];

  constructor() {
    this.name = '';
    this.tasks = [];
  }
}
