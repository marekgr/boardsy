import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {RegisterComponent} from './components/authentication/register/register.component';
import {LoginComponent} from './components/authentication/login/login.component';
import {BoardComponent} from './components/board/board.component';
import {LandingPageComponent} from './components/landing-page/landing-page/landing-page.component';
import {AuthGuard} from './services/authentication/auth.guard';
import {RegistrationCompletedComponent} from './components/authentication/register/registration-completed/registration-completed.component';

const routes: Routes = [
  {path: '', component: LandingPageComponent},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'board', component: BoardComponent},
  {path: 'registration-completed', component: RegistrationCompletedComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
