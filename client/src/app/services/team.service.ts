import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StoreService} from "./store.service";
import {Team} from "../model/team";

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  private readonly teamURL: string;

  constructor(private http: HttpClient,
              private store: StoreService) {
    this.teamURL = store.API_URL + '/team/';
  }

  public getTeam(id: string): Observable<Team> {
    return this.http.get<Team>(this.teamURL + id,
      {responseType: 'json'});
  }

  public getTeamsOfUser(id: string): Observable<Team[]> {
    return this.http.get<Team[]>(this.teamURL + 'findAll/ofUser/' + id,
      {responseType: 'json'});
  }

  public save(team: Team) {
    return this.http.post<Team>(this.teamURL + 'add',
      team);
  }

  public delete(team: Team) {
    const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'}), body: team};
    return this.http.delete(this.teamURL + 'delete', httpOptions);
  }

  public update(team: Team) {
    return this.http.post<Team>(this.teamURL + 'update', team);
  }
}
