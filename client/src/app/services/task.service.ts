import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {StoreService} from './store.service';
import {Task} from '../model/task';
import {Observable} from 'rxjs';
import {Comment} from '../model/comment';
import {User} from '../model/user';
import {Label} from '../model/label';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private readonly taskURL: string;

  constructor(private http: HttpClient,
              private store: StoreService) {
    this.taskURL = store.API_URL + '/tasks/';
  }

  public daleteTask(taskID: string) {
    return this.http.delete(this.taskURL + 'task-delete/' + taskID);
  }

  public createTask(task: Task, panelID: string) {
    return this.http.post(this.taskURL + panelID + '/new', task);
  }

  public refreshTask(taskId: string): Observable<Task> {
    return this.http.get<Task>(this.taskURL + taskId);
  }

  public updateTaskTitle(taskID: string, taskTitle: string) {
    return this.http.patch(this.taskURL + taskID + '/task-name', taskTitle);
  }

  public updateTaskLabel(taskID: string, taskLabel: Label) {
    return this.http.patch(this.taskURL + taskID + '/task-label', taskLabel);
  }

  public updateTaskPriority(taskID: string, taskPriority: string) {
    return this.http.patch(this.taskURL + taskID + '/task-priority', taskPriority);
  }

  public updateTaskStatus(taskID: string, taskStatus: string) {
    return this.http.patch(this.taskURL + taskID + '/task-status', taskStatus);
  }

  public updateTaskAssignedUser(taskID: string, taskAssignedUser: User) {
    return this.http.patch(this.taskURL + taskID + '/task-assigned-user', taskAssignedUser);
  }

  // COMMENTS
  public addComment(taskID: string, comment: Comment): Observable<Task> {
    return this.http.post<Task>(this.taskURL + taskID + '/comment-add', comment);
  }

  public deleteComment(commentID: string) {
    return this.http.delete(this.taskURL + 'comment-delete/' + commentID);
  }

  public updateComment(comment: Comment): Observable<Comment> {
    return this.http.post<Comment>(this.taskURL + 'comment-update', comment);
  }
}
