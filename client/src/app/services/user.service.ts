import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StoreService} from './store.service';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly userURL: string;

  constructor(private http: HttpClient,
              private store: StoreService) {
    this.userURL = this.store.API_URL + '/users/';
  }

  public search(match: string): Observable<User[]> {
    return this.http.post<User[]>(this.userURL + 'search-user', match);
  }

  public getUser(id: string): Observable<User> {
    return this.http.get<User>(this.userURL + id, {responseType: 'json'});
  }

  public getUserData(email: string, password: string): Observable<User> {
    return this.http.get<User>(this.userURL + 'getUserData/' + email + '/' + password,
      {responseType: 'json'});
  }

  public checkIfUserExists(email: string): Observable<boolean> {
    return this.http.get<boolean>(this.userURL + 'exists/' + email, {responseType: 'json'});
  }

  public register(user: User) {
    return this.http.post<User>(this.userURL + 'register', user);
  }

  public confirmRegistration(token: string): Observable<boolean> {
    return this.http.get<boolean>(this.userURL + 'confirm/registrationConfirm?token=' + token);
  }

  public updateUser(user: User) {
    return this.http.post<User>(this.userURL + 'update', user);
  }

  public save(user: User) {
    return this.http.post<User>(this.userURL + 'add', user);
  }

  public delete(user: User) {
    const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'}), body: user};
    return this.http.delete(this.userURL + 'delete', httpOptions);
  }
}
