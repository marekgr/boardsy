import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {Router} from '@angular/router';
import {SessionDataService} from './session-data.service';
import {catchError, tap} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(private tokenService: SessionDataService, private router: Router, private toastr: ToastrService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authReq = req;
    if (this.tokenService.getToken() != null) {
      authReq = req.clone({headers: req.headers.set('Authorization', 'Bearer' + this.tokenService.getToken())});
    }
    return next.handle(authReq)
      .pipe(
        tap((event: HttpEvent<any>) => {
        }),
        catchError((err) => {
          if (err.status === 401) {
            this.toastr.error('Brak dostępu')
            this.router.navigate(['']);
          }
          return throwError(err);
        })
      );
  }
}
