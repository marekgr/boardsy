import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {StoreService} from './store.service';
import {Board} from '../model/board';
import {User} from '../model/user';
import {Privileges} from '../model/privileges';
import {Label} from '../model/label';

export interface UserPrivileges {
  user: User;
  privileges: Privileges;
}

@Injectable({
  providedIn: 'root'
})

export class BoardService {

  private readonly boardURL: string;

  constructor(private http: HttpClient,
              private store: StoreService) {
    this.boardURL = store.API_URL + '/board/';
  }

  public create(board: Board) {
    return this.http.post<Board>(this.boardURL + 'create',
      board);
  }

  public delete(board: Board) {
    const httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'}), body: board};
    return this.http.delete(this.boardURL + 'delete', httpOptions);
  }

  public getBoard(id: string): Observable<Board> {
    return this.http.get<Board>(this.boardURL + id,
      {responseType: 'json'});
  }

  public getUserPrivileges(id: string): Observable<Privileges> {
    return this.http.get<Privileges>(this.boardURL + 'privileges/' + id,
      {responseType: 'json'});
  }

  public getBoardUsersPrivileges(id: string): Observable<UserPrivileges[]> {
    return this.http.get<UserPrivileges[]>(this.boardURL + id + '/team-privileges',
      {responseType: 'json'});
  }

  public getBoardsOfUser(id: string): Observable<Board[]> {
    return this.http.get<Board[]>(this.boardURL + 'findAll/ofUser/' + id,
      {responseType: 'json'});
  }

  public getBoardsOfTeam(teamId: string): Observable<Board[]> {
    return this.http.get<Board[]>(this.boardURL + 'findAll/ofTeam/' + teamId,
      {responseType: 'json'});
  }

  public checkForBoardUpdates(boardId: string, boardVersion: number): Observable<boolean> {
    return this.http.post<boolean>(this.boardURL + 'check-update-required/' + boardId, boardVersion);
  }

  public update(board: Board) {
    return this.http.post<Board>(this.boardURL + 'update', board);
  }

  public updatePanelsOrder(boardID: string, panelIDs: string[]): Observable<Board> {
    return this.http.patch<Board>(this.boardURL + boardID + '/update-panels-order', panelIDs);
  }

  public updateBoardUsersPrivileges(boardID: string, userPrivileges: UserPrivileges[]) {
    return this.http.patch(this.boardURL + boardID + '/update-team-privileges', userPrivileges);
  }

  public updateBoardLabels(boardID: string, labels: Label[]) {
    return this.http.patch(this.boardURL + boardID + '/update-labels', labels);
  }
}
