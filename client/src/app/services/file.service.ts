import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {StoreService} from './store.service';
import {Observable} from 'rxjs';
import {TaskFile} from '../model/taskfile';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  private readonly taskURL: string;

  constructor(private http: HttpClient,
              private store: StoreService) {
    this.taskURL = store.API_URL + '/tasks/';
  }

  public addFileToTask(taskID: string, taskFile: TaskFile, file: File): Observable<string> {
    const formData = new FormData();
    formData.append('fileName', taskFile.name);
    formData.append('extension', taskFile.extension);
    formData.append('createdByUserId', taskFile.createdBy.id);
    formData.append('createdOn', taskFile.createdOn);
    formData.append('preview', taskFile.preview);
    formData.append('file', file);
    return this.http.post<string>(this.taskURL + taskID + '/add-file', formData);
  }

  public getFileThumbnail(taskFileId: string): Observable<TaskFile> {
    return this.http.get<TaskFile>(this.taskURL + 'get-task-file/' + taskFileId,
      {responseType: 'json'});
  }

  public deleteFile(taskFileId: string) {
    return this.http.post(this.taskURL + 'delete-file/' + taskFileId,
      {responseType: 'json'});
  }
}
