import {Injectable} from '@angular/core';
import {Team} from '../model/team';
import {Board} from '../model/board';
import {User} from '../model/user';
import {Privileges} from '../model/privileges';
import {SessionDataService} from './session-data.service';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  public readonly API_URL: string = 'http://localhost:8080';

  public readonly OAUTH_CREDENTIALS: string = btoa('politechnika:politechnika');

  teams: Team[];

  currentBoard: Board;

  boards: Board[];

  user: User;

  privileges: Privileges;

  constructor(private sessionService: SessionDataService) {
    if (sessionService.getUser() != null) {
      this.user = this.sessionService.getUser();
    }
  }

  clear() {
    this.teams = null;
    this.boards = null;
    this.user = null;
    this.privileges = null;
  }

  accessToken() {
    return '?access_token=' + this.sessionService.getToken();
  }
}
