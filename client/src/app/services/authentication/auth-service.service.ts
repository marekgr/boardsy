import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {StoreService} from '../store.service';
import {SessionDataService} from '../session-data.service';

// tslint:disable
@Injectable()
export class AuthService {

  private readonly baseUrl: string

  constructor(private http: HttpClient,
              private store: StoreService,
              private sessionService: SessionDataService) {
    this.baseUrl = this.store + '/users/'
  }

  redirectURL: string;

  login(credentials): Observable<any> {
    const body = new HttpParams()
      .set('username', credentials.email)
      .set('password', credentials.password)
      .set('grant_type', 'password');

    const headers = {
      'Authorization': 'Basic ' + this.store.OAUTH_CREDENTIALS,
      'Content-type': 'application/x-www-form-urlencoded'
    };
    return this.http.post(this.store.API_URL + '/oauth/token', body, {headers});
  }

  checkLogin(): Observable<any> {
    return this.http.get(this.store.API_URL + '/users/checkLogin')
  }

  userLoggedIn(): boolean {
    return this.sessionService.getToken() != null;
  }

  logout() {
    this.sessionService.removeUser();
    this.sessionService.removeToken();
    this.store.clear();
  }
}
