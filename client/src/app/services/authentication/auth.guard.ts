import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { AuthService } from './auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    const url: string = state.url;
    const roles = route.data.roles as Array<string>;
    return this.checkLogin(url, roles);
  }

  checkLogin(url: string, roles: Array<string>) {
    if (this.authService.userLoggedIn()) {
      // if (this.authService.userCanAccessPageWithRoles(roles)) {
      //   return true;
      // } else {
      //   this.router.navigate(['/access-denied']);
      //   return false;
      // }
      return true;
    }

    // Storing attempted URL for redirecting
    this.authService.redirectURL = url;

    this.router.navigate(['/login']);
    return false;
  }

}
