import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {StoreService} from './store.service';
import {Panel} from '../model/panel';
import {Observable} from 'rxjs';

class TaskIdGroup {
  firstPanelTaskIds: string[];
  secondPanelTaskIds: string[];
}

@Injectable({
  providedIn: 'root'
})
export class PanelService {

  private readonly panelURL: string;

  constructor(private http: HttpClient,
              private store: StoreService) {
    this.panelURL = store.API_URL + '/panels/';
  }

  public createPanel(panel: Panel, boardID: string) {
    return this.http.post<Panel>(this.panelURL + 'board/' + boardID, panel);
  }

  public updateTasksOrder(panelID: string, taskIDs: string[]): Observable<Panel> {
    return this.http.patch<Panel>(this.panelURL + panelID + '/update-tasks-order', taskIDs);
  }

  public updateTasksBetweenPanels(firstPanelId: string, secondPanelId: string, firstTaskIDs: string[], secondTaskIds: string[]): Observable<Panel> {
    return this.http.patch<Panel>(this.panelURL + 'update-tasks-order-of/' + firstPanelId + '/and/' + secondPanelId, {
      fromPanelTaskIds: firstTaskIDs,
      toPanelTaskIds: secondTaskIds
    });
  }

  public deletePanel(panelID: string) {
    return this.http.delete(this.panelURL + 'delete/' + panelID);
  }
}
