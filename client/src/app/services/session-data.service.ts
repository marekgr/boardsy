import {Injectable} from '@angular/core';
import {User} from '../model/user';

const TOKEN_KEY = 'access_token';

const USERDATA_KEY = 'user_data';

@Injectable({
  providedIn: 'root'
})
export class SessionDataService {

  public saveToken(token: string) {
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return localStorage.getItem(TOKEN_KEY);
  }

  public removeToken() {
    localStorage.removeItem(TOKEN_KEY);
  }

  public saveUser(user: User) {
    delete user.password;
    localStorage.setItem(USERDATA_KEY, JSON.stringify(user));
  }

  public getUser(): User {
    return JSON.parse(localStorage.getItem(USERDATA_KEY));
  }

  public removeUser() {
    localStorage.removeItem(USERDATA_KEY);
  }
}
