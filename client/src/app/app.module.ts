import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { RegisterComponent } from './components/authentication/register/register.component';
import { LoginComponent } from './components/authentication/login/login.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { BoardComponent } from './components/board/board.component';

import { MatCheckboxModule } from '@angular/material/checkbox';
import { ToastrModule } from 'ngx-toastr';
import { SdHeaderComponent } from './components/common/sd-header/sd-header.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AuthService } from './services/authentication/auth-service.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { UserService } from './services/user.service';
import { SpinnerComponent } from './components/common/spinner/spinner.component';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { LandingPageComponent } from './components/landing-page/landing-page/landing-page.component';
import { BoardsTileComponent } from './components/dashboard/boards-tile/boards-tile.component';
import { TeamsTileComponent } from './components/dashboard/teams-tile/teams-tile.component';
import { NewBoardModalComponent } from './components/dashboard/new-board-modal/new-board-modal.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatRippleModule } from '@angular/material/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PanelComponent } from './components/board/panel/panel.component';
import { TaskComponent } from './components/board/panel/task/task.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CommentListComponent } from './components/board/panel/task/taskview-modal/comment-list/comment-list.component';
import { TaskViewModalComponent } from './components/board/panel/task/taskview-modal/task-view-modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ColorLabelComponent } from './components/common/color-label/color-label.component';
import { PriorityControlComponent } from './components/common/priority-control/priority-control.component';
import { NewTaskModalComponent } from './components/board/panel/task/new-task-modal/new-task-modal.component';
import { PrioritySelectorComponent } from './components/common/priority-selector/priority-selector.component';
import { CustomLabelsComponent } from './components/dashboard/new-board-modal/custom-labels/custom-labels.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { MatIconModule } from '@angular/material/icon';
import { ConfirmModalComponent } from './components/common/confirm-modal/confirm-modal.component';
import { ColorLabelSelectorComponent } from './components/common/color-label-selector/color-label-selector.component';
import { StatusSelectorComponent } from './components/common/status-selector/status-selector.component';
import { PrettyDatePipePipe } from './components/common/pretty-date-pipe.pipe';

import { SidePanelComponent } from './components/board/side-panel/side-panel.component';
import { LabelsManageModalComponent } from './components/board/side-panel/labels-manage-modal/labels-manage-modal.component';
import { TeamManagerModalComponent } from './components/board/side-panel/team-manager-modal/team-manager-modal.component';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { FileManagerComponent } from './components/common/file-manager/file-manager.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { FileListComponent } from './components/common/file-manager/file-list/file-list.component';
import { InterceptorService } from './services/interceptor.service';
import { PopoverModule } from 'ngx-smart-popover';
import { CollapseTemplateComponent } from './components/common/collapse-template/collapsetemplate.component';
import { NewTeamModalComponent } from './components/dashboard/teams-tile/new-team-modal/new-team-modal.component';
import { AvatarIconComponent } from './components/common/avatar-icon/avatar-icon.component';
import { NgxAutocompleteModule } from 'ngx-angular-autocomplete';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RegistrationCompletedComponent } from './components/authentication/register/registration-completed/registration-completed.component';
import { FileImageModalComponent } from './components/common/file-manager/file-image-modal/file-image-modal.component';
import {TeamBoardsComponent} from './components/dashboard/teams-tile/team-boards/team-boards.component';
import {Ng2ImgMaxModule} from 'ng2-img-max';


@NgModule({
  declarations: [
    AppComponent,
    SdHeaderComponent,
    RegisterComponent,
    LoginComponent,
    BoardComponent,
    DashboardComponent,
    SpinnerComponent,
    LandingPageComponent,
    BoardsTileComponent,
    TeamsTileComponent,
    NewBoardModalComponent,
    PanelComponent,
    TaskComponent,
    CommentListComponent,
    TaskViewModalComponent,
    ColorLabelComponent,
    PriorityControlComponent,
    NewTaskModalComponent,
    PrioritySelectorComponent,
    CustomLabelsComponent,
    ConfirmModalComponent,
    ColorLabelSelectorComponent,
    ColorLabelSelectorComponent,
    StatusSelectorComponent,
    PrettyDatePipePipe,
    SidePanelComponent,
    LabelsManageModalComponent,
    TeamManagerModalComponent,
    FileManagerComponent,
    FileListComponent,
    CollapseTemplateComponent,
    NewTeamModalComponent,
    AvatarIconComponent,
    RegistrationCompletedComponent,
    FileImageModalComponent,
    TeamBoardsComponent
  ],
  imports: [
    MatDialogModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatStepperModule,
    MatInputModule,
    ReactiveFormsModule,
    MatCardModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    MatSelectModule,
    MatMenuModule,
    MatAutocompleteModule,
    MatRippleModule,
    DragDropModule,
    MatTooltipModule,
    ColorPickerModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    NgxFileDropModule,
    Ng2ImgMaxModule,
    PopoverModule,
    NgxAutocompleteModule,
    MatProgressSpinnerModule
  ],
  providers: [
    AuthService,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
