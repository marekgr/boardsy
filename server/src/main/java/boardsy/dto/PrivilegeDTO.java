package boardsy.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import boardsy.domain.user.Privileges;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PrivilegeDTO {
    private boolean canCreateTask;

    private boolean canEditTask;

    private boolean canDeleteTask;

    private boolean canChangeOrder;

    private boolean canDeletePanel;

    private boolean canManageBoard;

    public PrivilegeDTO(Privileges privileges) {
        this.canEditTask = privileges.isTaskEditAllowed();
        this.canCreateTask = privileges.isTaskCreateAllowed();
        this.canChangeOrder = privileges.isOrderChangeAllowed();
        this.canDeleteTask = privileges.isTaskDeleteAllowed();
        this.canManageBoard = privileges.isManageBoardAllowed();
    }
}
