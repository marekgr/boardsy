package boardsy.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import boardsy.domain.files.TaskFile;
import boardsy.domain.user.User;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskFileDTO {
    private Long id;

    private String name;

    private Long fileID;

    private String extension;

    private User createdBy;

    private String createdOn;

    private String preview;

    public TaskFileDTO(TaskFile taskFile) {
        this.id = taskFile.getId();
        this.name = taskFile.getName();
        this.fileID = taskFile.getFileID();
        this.extension = taskFile.getExtension();
        this.createdBy = taskFile.getCreatedBy();
        this.createdOn = taskFile.getCreatedOn();
        this.preview = new String(taskFile.getPreview());
    }
}
