package boardsy.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PanelTransferTasksDTO {
    private List<Long> fromPanelTaskIds;

    private List<Long> toPanelTaskIds;
}
