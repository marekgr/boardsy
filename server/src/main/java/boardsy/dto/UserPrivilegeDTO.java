package boardsy.dto;

import lombok.*;
import boardsy.domain.user.User;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserPrivilegeDTO {
    private User user;

    private PrivilegeDTO privileges;
}
