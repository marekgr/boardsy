package boardsy.domain.files;

import lombok.*;
import boardsy.domain.tasks.Task;

import javax.persistence.*;

@Getter
@Setter
@Table(name = "files")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DbFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String fileType;

    private byte[] file;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "task_id")
    private Task task;
}
