package boardsy.domain.files;

import lombok.*;
import boardsy.domain.tasks.Task;
import boardsy.domain.user.User;

import javax.persistence.*;

@Getter
@Setter
@Table(name = "task_files_info")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Long fileID;

    private String extension;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "created_by_user_id")
    private User createdBy;

    private String createdOn;

    private byte[] preview;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "task_id")
    private Task task;
}
