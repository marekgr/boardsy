package boardsy.domain.board;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import boardsy.domain.tasks.Panel;
import boardsy.domain.user.User;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "boards")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Board implements RootAware<Board> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String tag;

    private Long tagNumber;

    @ManyToOne
    private User createdBy;

    private String createdOnDate;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "board", fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<Label> labels;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "board", fetch = FetchType.EAGER)
    @OrderColumn(name = "panels_index")
    @JsonManagedReference
    private List<Panel> panels;

    @ManyToMany
    @JoinTable(name = "board_users")
    private List<User> projectUsers;

    private Long teamID;

    @Version
    private int version;

    @Override
    public Board root() {
        return this;
    }
}