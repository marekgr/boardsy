package boardsy.domain.board;

public interface RootAware<T> {
    T root();
}
