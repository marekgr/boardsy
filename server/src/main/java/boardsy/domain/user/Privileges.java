package boardsy.domain.user;

import lombok.*;
import boardsy.domain.board.Board;
import boardsy.dto.PrivilegeDTO;

import javax.persistence.*;


@Builder
@Getter
@Setter
@Table(name = "privileges")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Privileges {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Board board;

    private boolean taskEditAllowed;

    private boolean taskCreateAllowed;

    private boolean orderChangeAllowed;

    private boolean taskDeleteAllowed;

    private boolean panelDeleteAllowed;

    private boolean manageBoardAllowed;

    public Privileges makeAdmin(Board board, User user) {
        return Privileges.builder()
                .board(board)
                .user(user)
                .taskCreateAllowed(true)
                .taskDeleteAllowed(true)
                .taskEditAllowed(true)
                .panelDeleteAllowed(true)
                .orderChangeAllowed(true)
                .manageBoardAllowed(true)
                .build();
    }

    public Privileges makeDefault(Board board, User user) {
        return Privileges.builder()
                .board(board)
                .user(user)
                .taskCreateAllowed(true)
                .taskEditAllowed(true)
                .taskDeleteAllowed(false)
                .panelDeleteAllowed(false)
                .orderChangeAllowed(false)
                .manageBoardAllowed(false)
                .build();
    }

    public Privileges fromPrivilegeDTO(PrivilegeDTO privilegeDTO) {
        this.taskCreateAllowed = privilegeDTO.isCanCreateTask();
        this.taskEditAllowed = privilegeDTO.isCanEditTask();
        this.taskDeleteAllowed = privilegeDTO.isCanDeleteTask();
        this.orderChangeAllowed = privilegeDTO.isCanChangeOrder();
        this.panelDeleteAllowed = privilegeDTO.isCanDeletePanel();
        this.manageBoardAllowed = privilegeDTO.isCanManageBoard();
        return this;
    }
}