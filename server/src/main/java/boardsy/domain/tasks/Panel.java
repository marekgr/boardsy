package boardsy.domain.tasks;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import boardsy.domain.board.Board;
import boardsy.domain.board.RootAware;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Table(name = "panels")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Panel implements RootAware<Board> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "panel", fetch = FetchType.EAGER)
    @OrderColumn(name = "tasks_index")
    @JsonManagedReference
    private List<Task> tasks;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "board_id")
    @JsonBackReference
    private Board board;


    @Override
    public Board root() {
        return board;
    }
}