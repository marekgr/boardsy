package boardsy.domain.tasks;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import boardsy.domain.board.Board;
import boardsy.domain.board.Label;
import boardsy.domain.board.RootAware;
import boardsy.domain.user.User;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Table(name = "tasks")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Task implements RootAware<Board> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String description;

    private String tag;

    @ManyToOne
    private Label label;

    private String status;

    private String priority;

    @ManyToOne
    private User assignedUser;

    @ManyToOne(fetch = FetchType.EAGER)
    private User createdByUser;

    private String createdOn;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "task", fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<Comment> comments;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "panel_id")
    @JsonBackReference
    private Panel panel;

    @ElementCollection
    private List<Long> fileIDs;

    @Override
    public Board root() {
        return panel.getBoard();
    }
}
