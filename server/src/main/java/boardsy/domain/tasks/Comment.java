package boardsy.domain.tasks;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import boardsy.domain.user.User;

import javax.persistence.Entity;
import javax.persistence.Table;

import javax.persistence.*;

@Getter
@Setter
@Table(name = "comments")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String content;

    @ManyToOne(fetch = FetchType.EAGER)
    private User commentedBy;

    private String commentedOn;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "task_id")
    @JsonBackReference
    private Task task;
}
