package boardsy.config;

import org.hibernate.integrator.spi.Integrator;
import org.hibernate.jpa.boot.spi.IntegratorProvider;
import boardsy.eventlisteners.RootAwareEventListenerIntegrator;

import java.util.Collections;
import java.util.List;

public class ClassImportIntegratorIntegratorProvider implements IntegratorProvider {

    @Override
    public List<Integrator> getIntegrators() {
        return Collections.singletonList(
                RootAwareEventListenerIntegrator.INSTANCE
        );
    }
}