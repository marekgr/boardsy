package boardsy.httpcontrolers.board;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;
import boardsy.domain.board.Board;
import boardsy.domain.board.Label;
import boardsy.domain.user.Privileges;
import boardsy.dto.PrivilegeDTO;
import boardsy.dto.UserPrivilegeDTO;
import boardsy.service.board.BoardService;
import boardsy.service.user.UserService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/board")
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class BoardController {

    @Autowired
    BoardService boardService;

    @Autowired
    UserService userService;

    @GetMapping
    public List<Board> getBoards() {
        return boardService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Board> getBoardById(@PathVariable Long id) {
        return boardService.findById(id)
                .map(it -> ResponseEntity.ok().body(it))
                .orElse(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

    @PostMapping("/create")
    public ResponseEntity<Board> createBoard(@RequestBody @Valid Board board, OAuth2Authentication auth) {
        return ResponseEntity.ok().body(boardService.create(board, auth));
    }

    @GetMapping("delete/{id}")
    public void deleteById(@PathVariable Long id) {
        boardService.deleteById(id);
    }

    @PostMapping("check-update-required/{id}")
    public ResponseEntity<Boolean> checkBoardUpdateRequired(@PathVariable Long id, @RequestBody int boardVersion, OAuth2Authentication auth) {
        if (userService.getUserPrivileges(auth, id) == null) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        return ResponseEntity.ok().body(boardService.boardVersionChanged(id, boardVersion));
    }

    @GetMapping("/privileges/{boardId}")
    public ResponseEntity<PrivilegeDTO> getUserPrivilege(@PathVariable Long boardId, OAuth2Authentication auth) {
        val privileges = userService.getUserPrivileges(auth, boardId);
        if (privileges == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return ResponseEntity.ok().body(new PrivilegeDTO(privileges));
    }

    @GetMapping("/findAll/ofUser/{id}")
    public List<Board> getBoardsByUserID(@PathVariable Long id) {
        return boardService.findAll()
                .stream()
                .filter(board -> board
                        .getProjectUsers()
                        .stream()
                        .anyMatch(user -> user.getId().equals(id)))
                .collect(Collectors.toList());
    }

    @GetMapping("/findAll/ofTeam/{id}")
    public List<Board> getBoardsByTeamID(@PathVariable Long id) {
        return boardService.findByTeamId(id);
    }

    @PatchMapping("/{boardID}/update-panels-order")
    public ResponseEntity<Board> patchPanelOrder(@PathVariable Long boardID, @RequestBody @Valid List<Long> panelIDs) {
        return ResponseEntity.ok()
                .body(boardService.updatePanelOrder(boardID, panelIDs));
    }

    @GetMapping("/{boardID}/team-privileges")
    public ResponseEntity<List<UserPrivilegeDTO>> getBoardUsersPrivileges(@PathVariable Long boardID, OAuth2Authentication auth) {
        if (!userService.getUserPrivileges(auth, boardID).isManageBoardAllowed()) {
            return ResponseEntity.ok().body(null);
        }

        List<UserPrivilegeDTO> UserPrivilegeDTO = boardService.getBoardUserPrivileges(boardID);

        if (UserPrivilegeDTO == null) {
            return ResponseEntity.ok().body(null);
        }
        return ResponseEntity.ok().body(UserPrivilegeDTO);
    }

    @PatchMapping("/{boardID}/update-team-privileges")
    public ResponseEntity patchBoardUsersPrivileges(@PathVariable Long boardID, @RequestBody @Valid List<UserPrivilegeDTO> userPrivileges, OAuth2Authentication auth) {
        if (!userService.getUserPrivileges(auth, boardID).isManageBoardAllowed()) {
            return ResponseEntity.notFound().build();
        }
        List<Privileges> privileges = boardService.updateBoardUserPrivileges(boardID, userPrivileges);

        if (privileges == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }

    @PatchMapping("/{boardID}/update-labels")
    public ResponseEntity patchBoardLabels(@PathVariable Long boardID, @RequestBody @Valid List<Label> labels, OAuth2Authentication auth) {
        if (!userService.getUserPrivileges(auth, boardID).isManageBoardAllowed()) {
            return ResponseEntity.notFound().build();
        }
        val updatedLabels = boardService.updateBoardLabels(boardID, labels);

        if (updatedLabels == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }

}
