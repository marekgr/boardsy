package boardsy.httpcontrolers.panel;

import javassist.NotFoundException;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import boardsy.domain.tasks.Panel;
import boardsy.dto.PanelTransferTasksDTO;
import boardsy.service.tasks.PanelService;
import boardsy.service.tasks.TaskService;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/panels")
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class PanelController {

    @Autowired
    TaskService taskService;

    @Autowired
    PanelService panelService;

    @PostMapping("/board/{boardID}")
    public ResponseEntity<Panel> createPanelInBoard(@PathVariable Long boardID, @RequestBody @Valid Panel panel) {
        return ResponseEntity.ok()
                .body(panelService.createPanel(boardID, panel));
    }

    @DeleteMapping("/delete/{panelID}")
    public void removePanel(@PathVariable Long panelID) {
        panelService.deletePanel(panelID);
    }

    @PatchMapping("/{panelID}/update-tasks-order")
    public ResponseEntity<Panel> patchTasksOrder(@PathVariable Long panelID, @RequestBody @Valid List<Long> taskIDs) {
        return ResponseEntity.ok()
                .body(panelService.updateTaskOrder(panelID, taskIDs));
    }

    @PatchMapping("/update-tasks-order-of/{fromPanelId}/and/{toPanelId}")
    public ResponseEntity<List<Panel>> patchTaskOrderBetweenPanels(
            @PathVariable Long fromPanelId,
            @PathVariable Long toPanelId,
            @RequestBody @Valid PanelTransferTasksDTO panelsTaskIds) throws NotFoundException {

        val fromPanelTasks = panelsTaskIds.getFromPanelTaskIds();
        val toPanelTasks = panelsTaskIds.getToPanelTaskIds();

        return ResponseEntity.ok()
                .body(panelService.updateTaskOrderBetweenPanels(
                        fromPanelId,
                        toPanelId,
                        fromPanelTasks,
                        toPanelTasks));
    }
}
