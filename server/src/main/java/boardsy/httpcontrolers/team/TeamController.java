package boardsy.httpcontrolers.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import boardsy.domain.team.Team;
import boardsy.service.team.TeamService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/team")
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TeamController {

    @Autowired
    TeamService teamService;

    @GetMapping
    public List<Team> getTeams() {
        return teamService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Team> getTeamById(@PathVariable Long id) {
        return teamService.findById(id)
                .map(it -> ResponseEntity.ok().body(it))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/add")
    public ResponseEntity<Team> addTeam(@RequestBody @Valid Team board) {
        teamService.save(board);
        return ResponseEntity.ok().body(board);
    }

    @GetMapping("delete/{id}")
    public void deleteById(@PathVariable Long id) {
        teamService.deleteById(id);
    }

    @GetMapping("/findAll/ofUser/{id}")
    public List<Team> getTeamsByUserID(@PathVariable Long id) {
        return teamService.findAll()
                .stream()
                .filter(team -> team.getUsers()
                        .stream()
                        .anyMatch(user -> user.getId().equals(id)))
                .filter(team -> team.getName() != null)
                .collect(Collectors.toList());
    }
}
