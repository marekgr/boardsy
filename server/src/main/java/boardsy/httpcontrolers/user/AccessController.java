package boardsy.httpcontrolers.user;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AccessController {

    @GetMapping("/public")
    public boolean welcomePublic() {return true;}

    @RolesAllowed({"ADMIN"})
    @GetMapping("/admin")
    public boolean welcomeAdmin() {return true;}

    @RolesAllowed({"USER"})
    @GetMapping("/user")
    public boolean welcomeUser() {return true;}

}
