package boardsy.httpcontrolers.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import boardsy.domain.user.User;
import boardsy.domain.user.VerificationToken;
import boardsy.service.MailService;
import boardsy.service.user.UserService;
import boardsy.service.user.VerificationService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;

@RequestMapping("/users")
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    VerificationService verificationService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    MailService mailService;

    @GetMapping
    public List<User> getUsers() {
        return userService.findAll();
    }


    @GetMapping("/{id}")
    public ResponseEntity<User> getUserByID(@PathVariable Long id) {
        return userService.findById(id)
                .map(student -> ResponseEntity.ok().body(student)).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/add")
    public User addUser(@RequestBody @Valid User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userService.save(user);
    }

    @PostMapping("/update")
    public User updateUser(@RequestBody @Valid User user) {
        return userService.save(user);
    }

    @PostMapping("/register")
    public ResponseEntity<String> registerUser(@RequestBody @Valid User user, HttpServletRequest request) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        mailService.sendVerification(userService.save(user));
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @RequestMapping(value = "/confirm/registrationConfirm", method = RequestMethod.GET)
    public boolean confirmRegistration(@RequestParam("token") String token) {
        VerificationToken verificationToken = verificationService.getVerificationToken(token);
        if (verificationToken == null) {
            System.out.println("Invalid");
            return false;
        }
        User user = verificationToken.getUser();
        Calendar calendar = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - calendar.getTime().getTime()) <= 0) {
            System.out.println("Expired");
            return false;
        }
        user.setActive(true);
        userService.save(user);
        return true;
    }

    @GetMapping("/exists/{email}")
    public boolean userExists(@PathVariable String email) {
        return userService.exists(email);
    }

    @PostMapping("/search-user")
    public List<User> searchUser(@RequestBody String match) {
        return userService.findAllByQuery(match);
    }

    @GetMapping("/getUserData/{email}/{password}")
    public ResponseEntity<User> getUserData(@PathVariable String email, @PathVariable String password) {
        return userService.findByEmailAndPassword(email, password)
                .map(user -> ResponseEntity.ok().body(user))
                .orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
    }

    @DeleteMapping("/delete")
    public void delete(@RequestBody @Valid User user) {
        userService.delete(user);
    }

    @GetMapping("/checkLogin")
    public Boolean checkLogin() {
        return true;
    }
}
