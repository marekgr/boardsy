package boardsy.httpcontrolers.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import boardsy.domain.board.Label;
import boardsy.domain.tasks.Task;
import boardsy.domain.user.User;
import boardsy.service.tasks.TaskService;

import javax.validation.Valid;

@RequestMapping("/tasks")
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TaskController {

    @Autowired
    TaskService taskService;

    @PostMapping("/{panelID}/new")
    public ResponseEntity<Task> createTaskInPanel(@PathVariable Long panelID, @RequestBody @Valid Task task) {
        return ResponseEntity.ok().body(taskService.createTask(panelID, task));
    }

    @GetMapping("/{taskId}")
    public ResponseEntity<Task> getTask(@PathVariable Long taskId) {
        return ResponseEntity.ok().body(taskService.getTask(taskId));
    }

    @DeleteMapping("/task-delete/{taskID}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteTask(@PathVariable Long taskID) {
        taskService.deleteTask(taskID);
    }

    @PatchMapping("/{taskID}/task-name")
    @ResponseStatus(HttpStatus.OK)
    public void patchTaskTitle(@PathVariable Long taskID, @RequestBody String title) {
        taskService.updateTaskTitle(taskID, title);
    }

    @PatchMapping("/{taskID}/task-label")
    @ResponseStatus(HttpStatus.OK)
    public void patchTaskLabel(@PathVariable Long taskID, @RequestBody Label label) {
        taskService.updateTaskLabel(taskID, label);
    }

    @PatchMapping("/{taskID}/task-priority")
    @ResponseStatus(HttpStatus.OK)
    public void patchTaskPriority(@PathVariable Long taskID, @RequestBody String priority) {
        taskService.updateTaskPriority(taskID, priority);
    }

    @PatchMapping("/{taskID}/task-status")
    @ResponseStatus(HttpStatus.OK)
    public void patchTaskStatus(@PathVariable Long taskID, @RequestBody String status) {
        taskService.updateTaskStatus(taskID, status);
    }

    @PatchMapping("/{taskID}/task-assigned-user")
    @ResponseStatus(HttpStatus.OK)
    public void patchTaskAssignedUser(@PathVariable Long taskID, @RequestBody User assignedUser) {
        taskService.updateTaskAssignedUser(taskID, assignedUser);
    }
}
