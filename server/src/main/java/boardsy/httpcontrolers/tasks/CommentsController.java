package boardsy.httpcontrolers.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import boardsy.domain.tasks.Comment;
import boardsy.domain.tasks.Task;
import boardsy.service.tasks.TaskService;

import javax.validation.Valid;

@RequestMapping("/tasks")
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CommentsController {

    @Autowired
    TaskService taskService;

    @PostMapping("/{taskID}/comment-add")
    public ResponseEntity<Task> addComment(@PathVariable Long taskID, @RequestBody @Valid Comment comment) {
        taskService.createComment(taskID, comment);
        return ResponseEntity.ok(taskService
                .createComment(taskID, comment)
                .getTask());
    }

    @PostMapping("/comment-update")
    public ResponseEntity<Comment> updateComment(@RequestBody @Valid Comment comment) {
        return ResponseEntity.ok(taskService
                .updateComment(comment));
    }

    @DeleteMapping("/comment-delete/{commentID}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteComment(@PathVariable Long commentID) {
        taskService.deleteComment(commentID);
    }
}
