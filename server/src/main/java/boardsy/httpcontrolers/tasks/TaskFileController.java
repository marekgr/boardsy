package boardsy.httpcontrolers.tasks;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import boardsy.domain.files.DbFile;
import boardsy.domain.files.TaskFile;
import boardsy.dto.TaskFileDTO;
import boardsy.service.tasks.TaskFileService;
import boardsy.service.user.UserService;

import java.io.FileNotFoundException;
import java.io.IOException;

@RequestMapping("/tasks")
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TaskFileController {

    @Autowired
    TaskFileService taskFileService;

    @Autowired
    UserService userService;

    @PostMapping("/{taskID}/add-file")
    public ResponseEntity<Long> uploadFile(
            @PathVariable Long taskID,
            @RequestParam("fileName") String name,
            @RequestParam("extension") String extension,
            @RequestParam("createdByUserId") Long createdByUserId,
            @RequestParam("createdOn") String createdOn,
            @RequestParam("preview") String preview,
            @RequestParam("file") MultipartFile file) throws IOException {
        val userCreator = userService.findById(createdByUserId).orElse(null);

        if (userCreator == null) {
            return ResponseEntity.badRequest().build();
        }

        val taskFile = TaskFile.builder()
                .name(name)
                .extension(extension)
                .createdBy(userCreator)
                .createdOn(createdOn)
                .preview(preview.getBytes())
                .build();

        return ResponseEntity.ok(taskFileService
                .addDbFile(taskID, taskFile, file).getId());
    }


    @GetMapping("/get-file/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable Long fileId) throws FileNotFoundException {
        DbFile dbFile = taskFileService.getDBFile(fileId);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getName() + "\"")
                .body(new ByteArrayResource(dbFile.getFile()));
    }

    @GetMapping("/get-task-file/{taskFileId}")
    public ResponseEntity<TaskFileDTO> getTaskFile(@PathVariable Long taskFileId) {
        return taskFileService.getTaskFile(taskFileId)
                .map(it -> ResponseEntity.ok().body(new TaskFileDTO(it)))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/delete-file/{taskFileId}")
    public ResponseEntity<Resource> deleteFile(@PathVariable Long taskFileId) throws FileNotFoundException {
        taskFileService.deleteTaskFile(taskFileId);
        return ResponseEntity.ok().build();
    }
}
