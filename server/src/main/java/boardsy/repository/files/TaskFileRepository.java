package boardsy.repository.files;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import boardsy.domain.files.TaskFile;

@Repository
public interface TaskFileRepository extends JpaRepository<TaskFile, Long> {
}