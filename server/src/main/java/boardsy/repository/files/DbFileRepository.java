package boardsy.repository.files;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import boardsy.domain.files.DbFile;

@Repository
public interface DbFileRepository extends JpaRepository<DbFile, Long> {
}