package boardsy.repository.tasks;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import boardsy.domain.tasks.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
}
