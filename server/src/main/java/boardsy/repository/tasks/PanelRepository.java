package boardsy.repository.tasks;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import boardsy.domain.tasks.Panel;

@Repository
public interface PanelRepository extends JpaRepository<Panel, Long> {
}