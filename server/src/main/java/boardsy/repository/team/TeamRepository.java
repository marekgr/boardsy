package boardsy.repository.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import boardsy.domain.team.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {
}