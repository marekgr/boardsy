package boardsy.repository.board;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import boardsy.domain.board.Board;
import boardsy.domain.user.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface BoardRepository extends JpaRepository<Board, Long> {

    @Query(value = "select version from boards where id = ?1", nativeQuery = true)
    Optional<Integer> getVersionOfBoardId(Long id);

    Optional<List<Board>> findAllByTeamID(Long id);

    Optional<List<Board>> findAllByProjectUsersContains(User user);
}