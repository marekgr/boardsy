package boardsy.repository.board;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import boardsy.domain.board.Label;

@Repository
public interface LabelRepository extends JpaRepository<Label, Long> {
}