package boardsy.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import boardsy.domain.board.Board;
import boardsy.domain.user.Privileges;
import boardsy.domain.user.User;

import java.util.List;

@Repository
public interface PrivilegeRepository extends JpaRepository<Privileges, Long> {

    Privileges findByUserAndBoard(User user, Board board);

    Privileges findByUserIdAndBoardId(Long userID, Long boardID);

    List<Privileges> findAllByBoard(Board board);
}