package boardsy.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import boardsy.domain.user.User;
import boardsy.domain.user.VerificationToken;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {

    VerificationToken findByToken(String token);

    VerificationToken findByUser(User appUser);
}
