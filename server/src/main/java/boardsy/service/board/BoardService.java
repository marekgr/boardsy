package boardsy.service.board;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import boardsy.domain.board.Board;
import boardsy.domain.board.Label;
import boardsy.domain.tasks.Panel;
import boardsy.domain.user.Privileges;
import boardsy.domain.user.User;
import boardsy.dto.PrivilegeDTO;
import boardsy.dto.UserPrivilegeDTO;
import boardsy.repository.board.BoardRepository;
import boardsy.repository.board.LabelRepository;
import boardsy.repository.tasks.PanelRepository;
import boardsy.repository.tasks.TaskRepository;
import boardsy.repository.team.TeamRepository;
import boardsy.repository.user.PrivilegeRepository;
import boardsy.repository.user.UserRepository;
import boardsy.service.user.AuthUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BoardService {

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private PanelRepository panelRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Autowired
    private LabelRepository labelRepository;

    @Autowired
    private TaskRepository taskRepository;

    public Boolean boardVersionChanged(Long boardId, int version) {
        return boardRepository.getVersionOfBoardId(boardId)
                .map(boardVersion -> boardVersion != version)
                .orElseThrow(() -> new NoSuchElementException("No board found for this id"));
    }

    public Board create(Board board, OAuth2Authentication auth) {
        User createdBy = userRepository.findByEmail(AuthUtils.extractCurrentEmail(auth));
        board.setTagNumber(0L);
        board.getLabels().forEach(label -> label.setBoard(board));
        board.setCreatedBy(createdBy);

        Board createdBoard = boardRepository.save(board);

        setDefaultBoardPrivileges(createdBoard);

        return createdBoard;
    }

    private void setDefaultBoardPrivileges(Board board) {
        board.getProjectUsers()
                .stream()
                .filter(user -> !user.getId().equals(board.getCreatedBy().getId()))
                .forEach(user -> {
                            privilegeRepository.save(new Privileges()
                                    .makeDefault(board, user));
                        }
                );
        privilegeRepository.save(new Privileges()
                .makeAdmin(board, board.getCreatedBy()));
    }

    public Board updatePanelOrder(Long boardID, List<Long> panelIDs) {
        return boardRepository.findById(boardID).map(
                board -> {
                    List<Panel> panels = new ArrayList<>();
                    panelIDs.forEach(id -> panelRepository.findById(id)
                            .ifPresent(panel -> {
                                panel.setBoard(board);
                                panels.add(panel);
                            }));
                    board.setPanels(panels);
                    return boardRepository.save(board);
                }
        ).orElse(null);
    }

    public List<UserPrivilegeDTO> getBoardUserPrivileges(Long boardID) {
        return boardRepository.findById(boardID).map(
                board -> privilegeRepository.findAllByBoard(board)
                        .stream()
                        .map(privileges -> UserPrivilegeDTO.builder()
                                .user(privileges.getUser())
                                .privileges(new PrivilegeDTO(privileges))
                                .build())
                        .collect(Collectors.toList())
        ).orElse(null);
    }

    public List<Privileges> updateBoardUserPrivileges(Long boardID, List<UserPrivilegeDTO> userPrivileges) {
        return userPrivileges.stream().map(
                userPrivilegeDTO -> {
                    Privileges privileges = privilegeRepository
                            .findByUserIdAndBoardId(userPrivilegeDTO.getUser().getId(), boardID);
                    privileges.setTaskCreateAllowed(userPrivilegeDTO.getPrivileges().isCanCreateTask());
                    privileges.setTaskEditAllowed(userPrivilegeDTO.getPrivileges().isCanEditTask());
                    privileges.setTaskDeleteAllowed(userPrivilegeDTO.getPrivileges().isCanDeleteTask());
                    privileges.setOrderChangeAllowed(userPrivilegeDTO.getPrivileges().isCanChangeOrder());
                    privileges.setPanelDeleteAllowed(userPrivilegeDTO.getPrivileges().isCanDeletePanel());
                    privileges.setManageBoardAllowed(userPrivilegeDTO.getPrivileges().isCanManageBoard());
                    return privilegeRepository.save(privileges);
                }).collect(Collectors.toList());
    }

    public List<Label> updateBoardLabels(Long boardID, List<Label> labels) {
        val board = boardRepository.findById(boardID).orElse(null);
        if (board == null) {
            return null;
        }

        val labelsToRemove = board.getLabels().stream()
                .filter(label ->
                        labels.stream()
                                .noneMatch(it -> it.getId().equals(label.getId())))
                .collect(Collectors.toList());

        labels.forEach(label -> label.setBoard(board));
        board.setLabels(labels);

        val updatedLabels = boardRepository.save(board).getLabels();

        val tasksToUpdate = taskRepository.findAllByLabelIdIn(labelsToRemove.stream()
                .map(Label::getId)
                .collect(Collectors.toList()));

        tasksToUpdate.ifPresent(tasks ->
                tasks.forEach(task -> {
                    task.setLabel(null);
                    taskRepository.save(task);
                }));

        labelsToRemove.forEach(label -> labelRepository.deleteById(label.getId()));

        return updatedLabels;
    }

    public Optional<Board> findById(Long id) {
        return boardRepository.findById(id);
    }

    public List<Board> findAll() {
        return boardRepository.findAll();
    }

    public void deleteById(Long id) {
        boardRepository.deleteById(id);
    }

    public List<Board> findByTeamId(Long id) {
        return boardRepository.findAllByTeamID(id)
                .orElse(new ArrayList<>());
    }
}