package boardsy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import boardsy.domain.user.User;
import boardsy.service.user.VerificationService;

import java.util.UUID;

@Service
public class MailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    VerificationService verificationService;

    @Async
    public void sendVerification(User user) {
        String token = UUID.randomUUID().toString();
        verificationService.createVerificationToken(user, token);
        sendConfirmationEmail(user.getEmail(), token);
    }

    private void sendConfirmationEmail(String email, String token) {
        String subject = "Rejestracja pomyślna";
        String message = "Dziękujemy za rejestracje w naszym systemie! Kliknij w link poniżej aby potwierdzić tożsamość i zakończyć proces rejestracji\n";

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject(subject);
        mailMessage.setText(message + "http://localhost:4200/registration-completed?token=" + token);
        javaMailSender.send(mailMessage);
    }
}
