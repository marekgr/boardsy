package boardsy.service.tasks;

import javassist.NotFoundException;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import boardsy.domain.tasks.Panel;
import boardsy.domain.tasks.Task;
import boardsy.repository.board.BoardRepository;
import boardsy.repository.tasks.PanelRepository;
import boardsy.repository.tasks.TaskRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class PanelService {

    @Autowired
    private PanelRepository panelRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private BoardRepository boardRepository;


    public Panel createPanel(Long boardID, Panel panel) {
        return boardRepository.findById(boardID).map(
                board -> {
                    panel.setBoard(board);
                    board.getPanels().add(panel);
                    List<Panel> panels = boardRepository.save(board).getPanels();
                    return boardRepository.save(board)
                            .getPanels()
                            .stream()
                            .reduce((first, last) -> last)
                            .orElse(null);
                }
        ).orElse(null);
    }

    public void deletePanel(Long panelID) {
        panelRepository.findById(panelID).ifPresent(
                panel -> {
                    panel.getBoard().getPanels().removeIf(item -> item.getId() == panel.getId());
                    panelRepository.deleteById(panelID);
                }
        );
    }

    public Panel updateTaskOrder(Long panelID, List<Long> taskIDs) {
        return panelRepository.findById(panelID).map(
                panel -> {
                    List<Task> tasks = new ArrayList<>();
                    taskIDs.forEach(id -> taskRepository.findById(id)
                            .ifPresent(task -> {
                                task.setPanel(panel);
                                tasks.add(task);
                            }));
                    panel.setTasks(tasks);
                    return panelRepository.save(panel);
                }
        ).orElse(null);
    }

    public List<Panel> updateTaskOrderBetweenPanels(Long fromPanelId, Long toPanelId, List<Long> fromPanelTaskIds, List<Long> toPanelTaskIds) throws NotFoundException {
        val firstPanel = panelRepository.findById(fromPanelId).orElse(null);
        val secondPanel = panelRepository.findById(toPanelId).orElse(null);
        if (firstPanel == null || secondPanel == null) {
            throw new NotFoundException("No panel found");
        }
        val firstPanelTasks = new ArrayList<Task>();
        val secondPanelTasks = new ArrayList<Task>();

        taskRepository.findAllById(fromPanelTaskIds)
                .forEach(task -> {
                    task.setPanel(firstPanel);
                    firstPanelTasks.add(task);
                });

        taskRepository.findAllById(toPanelTaskIds)
                .forEach(task -> {
                    task.setPanel(secondPanel);
                    secondPanelTasks.add(task);
                });

        firstPanel.setTasks(firstPanelTasks);
        secondPanel.setTasks(secondPanelTasks);
        return panelRepository.saveAll(Arrays.asList(firstPanel, secondPanel));
    }

}
