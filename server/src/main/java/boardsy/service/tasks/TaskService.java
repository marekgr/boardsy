package boardsy.service.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import boardsy.domain.board.Board;
import boardsy.domain.board.Label;
import boardsy.domain.tasks.Comment;
import boardsy.domain.tasks.Panel;
import boardsy.domain.tasks.Task;
import boardsy.domain.user.User;
import boardsy.repository.board.BoardRepository;
import boardsy.repository.tasks.CommentRepository;
import boardsy.repository.tasks.PanelRepository;
import boardsy.repository.tasks.TaskRepository;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private PanelRepository panelRepository;

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private CommentRepository commentRepository;


    public Task getTask(Long taskId) {
        return taskRepository.findById(taskId).orElse(null);
    }

    public Task createTask(Long panelID, Task task) {
        return panelRepository.findById(panelID)
                .map(
                        panel -> {
                            task.setPanel(panel);
                            task.setTag(getCurrentTaskTagNumber(panel));
                            panel.getTasks().add(task);
                            return panelRepository.save(panel)
                                    .getTasks()
                                    .stream()
                                    .reduce((first, last) -> last)
                                    .orElse(null);
                        }
                ).orElse(null);
    }

    public void deleteTask(Long taskID) {
        taskRepository.findById(taskID).ifPresent(
                task -> {
                    task.getPanel().getTasks().removeIf(item -> item.getId() == task.getId());
                    taskRepository.deleteById(taskID);
                }
        );
    }

    public Task updateTaskTitle(Long taskID, String title) {
        return taskRepository.findById(taskID)
                .map(task -> {
                            task.setTitle(title);
                            return taskRepository.save(task);
                        }
                ).orElse(null);
    }

    public Task updateTaskPriority(Long taskID, String priority) {
        return taskRepository.findById(taskID)
                .map(task -> {
                            task.setPriority(priority);
                            return taskRepository.save(task);
                        }
                ).orElse(null);
    }

    public Task updateTaskLabel(Long taskID, Label label) {
        return taskRepository.findById(taskID)
                .map(task -> {
                            task.setLabel(label);
                            return taskRepository.save(task);
                        }
                ).orElse(null);
    }

    public Task updateTaskStatus(Long taskID, String status) {
        return taskRepository.findById(taskID)
                .map(task -> {
                            task.setStatus(status);
                            return taskRepository.save(task);
                        }
                ).orElse(null);
    }

    public Task updateTaskAssignedUser(Long taskID, User user) {
        return taskRepository.findById(taskID)
                .map(task -> {
                            task.setAssignedUser(user);
                            return taskRepository.save(task);
                        }
                ).orElse(null);
    }

    public Comment createComment(Long taskID, Comment comment) {
        taskRepository.findById(taskID)
                .ifPresent(comment::setTask);
        return commentRepository.save(comment);
    }

    public void deleteComment(Long commentID) {
        commentRepository.deleteById(commentID);
    }

    public Comment updateComment(Comment comment) {
        Comment newComment = commentRepository.findById(comment.getId()).get();
        newComment.setContent(comment.getContent());
        return commentRepository.save(newComment);
    }

    private String getCurrentTaskTagNumber(Panel panel) {
        Board board = panel.getBoard();
        board.setTagNumber(board.getTagNumber() + 1);
        boardRepository.save(board);
        return board.getTag() + "-" + String.format("%04d", board.getTagNumber());
    }
}
