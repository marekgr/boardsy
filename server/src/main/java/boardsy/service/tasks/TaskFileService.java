package boardsy.service.tasks;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import boardsy.domain.files.DbFile;
import boardsy.domain.files.TaskFile;
import boardsy.domain.tasks.Task;
import boardsy.repository.files.DbFileRepository;
import boardsy.repository.files.TaskFileRepository;
import boardsy.repository.tasks.TaskRepository;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TaskFileService {

    @Autowired
    DbFileRepository dbFileRepository;

    @Autowired
    TaskFileRepository taskFileRepository;

    @Autowired
    TaskRepository taskRepository;

    public TaskFile addDbFile(Long taskID, TaskFile taskFile, MultipartFile file) throws IOException {
        Task task = taskRepository.findById(taskID).orElse(null);

        if (task == null) {
            return null;
        }

        val dbFile = dbFileRepository.save(DbFile.builder()
                .file(file.getBytes())
                .fileType(file.getContentType())
                .task(task)
                .name(file.getOriginalFilename())
                .build());

        taskFile.setFileID(dbFile.getId());
        taskFile.setTask(task);

        val resultTaskFile = taskFileRepository.save(taskFile);

        task.getFileIDs().add(resultTaskFile.getId());
        taskRepository.save(task);

        return resultTaskFile;
    }

    public DbFile getDBFile(Long fileId) throws FileNotFoundException {
        return dbFileRepository.findById(fileId)
                .orElseThrow(() -> new FileNotFoundException("File not found with id " + fileId));
    }

    public Optional<TaskFile> getTaskFile(Long taskFileID) {
        return taskFileRepository.findById(taskFileID);
    }

    public boolean deleteTaskFile(Long taskFileID) throws FileNotFoundException {
        val taskFile = taskFileRepository.findById(taskFileID);
        return taskFile.map(file -> {
            dbFileRepository.deleteById(file.getFileID());
            val updatedTask = file.getTask();
            updatedTask.setFileIDs(
                    updatedTask
                            .getFileIDs()
                            .stream()
                            .filter(it -> !it.equals(taskFileID))
                            .collect(Collectors.toList()));

            taskRepository.save(updatedTask);
            taskFileRepository.delete(file);
            return true;
        }).orElseThrow(() -> new FileNotFoundException("File not found with id " + taskFileID));
    }
}
