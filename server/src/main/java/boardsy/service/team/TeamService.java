package boardsy.service.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import boardsy.domain.team.Team;
import boardsy.repository.team.TeamRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TeamService {

    @Autowired
    private TeamRepository teamRepository;

    public void save(Team team) {
        teamRepository.save(team);
    }

    public void saveAll(List<Team> teams) {
        teamRepository.saveAll(teams);
    }

    public Optional<Team> findById(Long id) {
        return teamRepository.findById(id);
    }

    public boolean existsById(Long id) {
        return teamRepository.existsById(id);
    }

    public List<Team> findAll() {
        return teamRepository.findAll();
    }

    public List<Team> findAllById(List<Long> ids) {
        return teamRepository.findAllById(ids);
    }

    public long count() {
        return teamRepository.count();
    }

    public void deleteById(Long id) {
        teamRepository.deleteById(id);
    }

    public void delete(Team team) {
        teamRepository.delete(team);
    }

    public void deleteAll(List<Team> teams) {
        teamRepository.deleteAll(teams);
    }

    public void deleteAll() {
        teamRepository.deleteAll();
    }

}
