package boardsy.service.user;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import boardsy.domain.user.User;
import boardsy.domain.user.VerificationToken;
import boardsy.repository.user.VerificationTokenRepository;

@Service
public class VerificationService {

    @Autowired
    VerificationTokenRepository verificationTokenRepository;

    public void createVerificationToken(final User user, final String token) {
        val myToken = new VerificationToken(user, token);
        verificationTokenRepository.save(myToken);
    }

    public VerificationToken getVerificationToken(String verificationToken) {
        return verificationTokenRepository.findByToken(verificationToken);
    }
}
