package boardsy.service.user;

import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.util.LinkedHashMap;

public class AuthUtils {

    public static String extractCurrentEmail(OAuth2Authentication auth) {
        if (auth == null) {
            throw new IllegalStateException("Cannot get current authentication object");
        }
        if (auth.getUserAuthentication().getDetails() == null) {
            return null;
        }
        if (auth.getUserAuthentication().getDetails() instanceof LinkedHashMap) {
            return (String) ((LinkedHashMap) auth.getUserAuthentication().getDetails()).get("username");
        }
        if (auth.getUserAuthentication().getDetails() instanceof String) {
            return String.valueOf(auth.getDetails());
        }
        return null;
    }

}
