package boardsy.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import boardsy.domain.user.Privileges;
import boardsy.domain.user.User;
import boardsy.repository.board.BoardRepository;
import boardsy.repository.user.PrivilegeRepository;
import boardsy.repository.user.UserRepository;
import boardsy.repository.user.VerificationTokenRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BoardRepository boardRepository;

    @Autowired
    PrivilegeRepository privilegeRepository;

    @Autowired
    VerificationTokenRepository verificationTokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public void delete(User user) {
        userRepository.delete(user);
    }

    public boolean exists(String email) {
        return !userRepository.findUserByEmail(email).isEmpty();
    }

    public List<User> findAllByQuery(String match) {
        return userRepository.findAll()
                .stream()
                .filter(user -> user.getUsername().toLowerCase().contains(match.toLowerCase()) || user.getEmail().toLowerCase().contains(match.toLowerCase()))
                .collect(Collectors.toList());
    }

    public Optional<User> findByEmailAndPassword(String email, String password) {
        User user = userRepository.findByEmail(email);
        if (passwordEncoder.matches(password, user.getPassword()) && user.isActive()) {
            return Optional.of(user);
        } else {
            return Optional.empty();
        }
    }

    public Privileges getUserPrivileges(OAuth2Authentication auth, Long boardID) {
        User user = userRepository.findByEmail(AuthUtils.extractCurrentEmail(auth));

        return boardRepository.findById(boardID)
                .map(board -> privilegeRepository.findByUserAndBoard(user, board)).orElse(null);
    }
}
