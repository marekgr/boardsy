-- Users
INSERT INTO users(id, active, email, password, username)
VALUES (1, true, 'aim95@wp.pl', '$2a$10$wlVFznAE1lVfVcU0o0tG8eR5XIII0xEmJ.0Aeq3LVtuzsK6qQhQ6C', 'RiverX');
INSERT INTO users(id, active, email, password, username)
VALUES (2, true, 'mail1@wp.pl', '$2a$10$wlVFznAE1lVfVcU0o0tG8eR5XIII0xEmJ.0Aeq3LVtuzsK6qQhQ6C', 'Dawid_K');
INSERT INTO users(id, active, email, password, username)
VALUES (3, true, 'mail2@wp.pl', '$2a$10$wlVFznAE1lVfVcU0o0tG8eR5XIII0xEmJ.0Aeq3LVtuzsK6qQhQ6C', 'Piotr_A');
INSERT INTO users(id, active, email, password, username)
VALUES (4, true, 'mail3@wp.pl', '$2a$10$wlVFznAE1lVfVcU0o0tG8eR5XIII0xEmJ.0Aeq3LVtuzsK6qQhQ6C', 'James_B');
INSERT INTO users(id, active, email, password, username)
VALUES (5, true, 'mail4@wp.pl', '$2a$10$wlVFznAE1lVfVcU0o0tG8eR5XIII0xEmJ.0Aeq3LVtuzsK6qQhQ6C', 'Lukasz_H');
INSERT INTO users(id, active, email, password, username)
VALUES (6, true, 'mail5@wp.pl', '$2a$10$wlVFznAE1lVfVcU0o0tG8eR5XIII0xEmJ.0Aeq3LVtuzsK6qQhQ6C', 'Artur_K');
INSERT INTO users(id, active, email, password, username)
VALUES (7, true, 'mail6@wp.pl', '$2a$10$wlVFznAE1lVfVcU0o0tG8eR5XIII0xEmJ.0Aeq3LVtuzsK6qQhQ6C', 'Homer_S');
ALTER SEQUENCE public.users_id_seq RESTART WITH 8;
-- Teams
INSERT INTO teams(id, name)
VALUES (1, 'Microsoft Team');
INSERT INTO teams(id, name)
VALUES (2, 'Cukieraski');
INSERT INTO teams(id, name)
VALUES (3, 'Drużyna Pierścionka');
ALTER SEQUENCE public.teams_id_seq RESTART WITH 4;

INSERT INTO teams_users(team_id, users_id)
VALUES (1, 1);
INSERT INTO teams_users(team_id, users_id)
VALUES (1, 2);
INSERT INTO teams_users(team_id, users_id)
VALUES (1, 4);
INSERT INTO teams_users(team_id, users_id)
VALUES (2, 1);
INSERT INTO teams_users(team_id, users_id)
VALUES (2, 5);
INSERT INTO teams_users(team_id, users_id)
VALUES (2, 6);
INSERT INTO teams_users(team_id, users_id)
VALUES (2, 7);
INSERT INTO teams_users(team_id, users_id)
VALUES (3, 4);
INSERT INTO teams_users(team_id, users_id)
VALUES (3, 6);
INSERT INTO teams_users(team_id, users_id)
VALUES (3, 7);

-- Boards
INSERT INTO boards(id, created_on_date, name, tag, tag_number, teamid, version, created_by_id)
VALUES (1, '06-06-2020', 'Projekt Beaver', 'BR', 0, 1, 0, 1);
INSERT INTO boards(id, created_on_date, name, tag, tag_number, teamid, version, created_by_id)
VALUES (2, '01-05-2020', 'Projekt MSDNAView', 'MSDNA', 0, 1, 0, 1);
ALTER SEQUENCE public.boards_id_seq RESTART WITH 3;

-- Boards users
INSERT INTO board_users(board_id, project_users_id)
VALUES (1, 1);
INSERT INTO board_users(board_id, project_users_id)
VALUES (1, 2);
INSERT INTO board_users(board_id, project_users_id)
VALUES (1, 4);

INSERT INTO board_users(board_id, project_users_id)
VALUES (2, 1);
INSERT INTO board_users(board_id, project_users_id)
VALUES (2, 2);
INSERT INTO board_users(board_id, project_users_id)
VALUES (2, 4);

-- Board users privileges
INSERT INTO privileges(id, manage_board_allowed, order_change_allowed, panel_delete_allowed, task_create_allowed,
                       task_delete_allowed, task_edit_allowed, board_id, user_id)
VALUES (1, true, true, true, true, true, true, 1, 1);
INSERT INTO privileges(id, manage_board_allowed, order_change_allowed, panel_delete_allowed, task_create_allowed,
                       task_delete_allowed, task_edit_allowed, board_id, user_id)
VALUES (2, false, true, false, true, false, true, 1, 2);
INSERT INTO privileges(id, manage_board_allowed, order_change_allowed, panel_delete_allowed, task_create_allowed,
                       task_delete_allowed, task_edit_allowed, board_id, user_id)
VALUES (3, false, true, false, true, false, true, 1, 4);

INSERT INTO privileges(id, manage_board_allowed, order_change_allowed, panel_delete_allowed, task_create_allowed,
                       task_delete_allowed, task_edit_allowed, board_id, user_id)
VALUES (4, true, true, true, true, true, true, 2, 1);
INSERT INTO privileges(id, manage_board_allowed, order_change_allowed, panel_delete_allowed, task_create_allowed,
                       task_delete_allowed, task_edit_allowed, board_id, user_id)
VALUES (5, false, true, false, true, false, true, 2, 2);
INSERT INTO privileges(id, manage_board_allowed, order_change_allowed, panel_delete_allowed, task_create_allowed,
                       task_delete_allowed, task_edit_allowed, board_id, user_id)
VALUES (6, false, true, false, true, false, true, 2, 4);
ALTER SEQUENCE public.privileges_id_seq RESTART WITH 7;

-- Labels
INSERT INTO labels(id, color, text, board_id)
VALUES (1, 'red', 'Implement', 1);
INSERT INTO labels(id, color, text, board_id)
VALUES (2, 'green', 'Analysis', 1);
INSERT INTO labels(id, color, text, board_id)
VALUES (3, 'blue', 'Backend fix', 1);
INSERT INTO labels(id, color, text, board_id)
VALUES (4, 'orange', 'Frontend fix', 1);

INSERT INTO labels(id, color, text, board_id)
VALUES (5, 'red', 'Reference material', 2);
INSERT INTO labels(id, color, text, board_id)
VALUES (6, 'green', 'Retexture', 2);
INSERT INTO labels(id, color, text, board_id)
VALUES (7, 'blue', 'Mesh fix', 2);
INSERT INTO labels(id, color, text, board_id)
VALUES (8, 'grey', 'Collision', 2);
ALTER SEQUENCE public.labels_id_seq RESTART WITH 9;

-- Panels
INSERT INTO panels(id, name, board_id, panels_index)
VALUES (1, 'TO-DO', 1, 0);
INSERT INTO panels(id, name, board_id, panels_index)
VALUES (2, 'Dev team', 1, 1);
INSERT INTO panels(id, name, board_id, panels_index)
VALUES (3, 'UI/UX Team', 1, 2);

INSERT INTO panels(id, name, board_id, panels_index)
VALUES (4, 'Material Reference team', 2, 0);
INSERT INTO panels(id, name, board_id, panels_index)
VALUES (5, 'Modeling - texturing team', 2, 1);
INSERT INTO panels(id, name, board_id, panels_index)
VALUES (6, 'Editing', 2, 2);
INSERT INTO panels(id, name, board_id, panels_index)
VALUES (7, 'Testing in-game', 2, 3);
INSERT INTO panels(id, name, board_id, panels_index)
VALUES (8, 'Released and pushed', 2, 4);
ALTER SEQUENCE public.panels_id_seq RESTART WITH 9;

-- PRIORITIES: LOW MED HIGH IMPROV FIX
-- STATUSES
-- BACKLOG
-- IN PROGRESS
-- CODE REVIEW
-- TESTING DONE
-- TO FIX

-- Tasks
-- Board 1
INSERT INTO tasks(id, created_on, description, priority, status, tag, title, assigned_user_id, created_by_user_id, label_id, panel_id, tasks_index)
VALUES (1, '01-06-2020', 'Do utworzenia komponent logowania, wykorzystać Angular-Material w jak największym stopniu.', 'MED', 'BACKLOG', 'BR-001', 'Komponent logowania', 1, 1, 1, 1, 0);
INSERT INTO tasks(id, created_on, description, priority, status, tag, title, assigned_user_id, created_by_user_id, label_id, panel_id, tasks_index)
VALUES (2, '04-06-2020', 'Utworzyć modal potwierdzający zalogowanie. Utworzyć również modal wyskakujący w przypadku niepoprawnych danych logowania', 'MED', 'BACKLOG', 'BR-002', 'Modal Success Login', 2, 1, 2, 1, 1);
INSERT INTO tasks(id, created_on, description, priority, status, tag, title, assigned_user_id, created_by_user_id, label_id, panel_id, tasks_index)
VALUES (3, '01-06-2020', 'Zaprojektować wygląd modalki potwierdzającej logowanie. Zgodna z Material Design', 'MED', 'IN PROGRESS', 'BR-003', 'Design modala Success Login', 2, 1, 2, 3, 0);
INSERT INTO tasks(id, created_on, description, priority, status, tag, title, assigned_user_id, created_by_user_id,label_id, panel_id, tasks_index)
VALUES (4, '05-06-2020', 'Zaprojektować wygląd formatki logowania, zgodny z Material Design. Formatka powinna być responsywna, zawierać odnosnik do rejestracji', 'MED', 'IN PROGRESS', 'BR-004', 'Design formatki logowania', 1, 1, 2, 3, 1);
INSERT INTO tasks(id, created_on, description, priority, status, tag, title, assigned_user_id, created_by_user_id,label_id, panel_id, tasks_index)
VALUES (5, '05-06-2020', 'Implementacja procesu logowania po stronie backend. Wykorzystac gotowe biblioteki. Logowanie basic', 'HIGH', 'IN PROGRESS', 'BR-005', 'Implementacja procesu logowania', 1, 1, 1, 2, 0);
INSERT INTO tasks(id, created_on, description, priority, status, tag, title, assigned_user_id, created_by_user_id,label_id, panel_id, tasks_index)
VALUES (6, '05-06-2020', 'Analiza wykorzystania oAuth', 'HIGH', 'BACKLOG', 'BR-006', 'Implementacja procesu logowania', 1, 1, 2, 1, 2);



-- Board 2
INSERT INTO tasks(id, created_on, description, priority, status, tag, title, assigned_user_id, created_by_user_id, label_id, panel_id, tasks_index) VALUES
(7, '01-06-2020', '[Materiał poglądowy] - Załączyć do tego zgłoszenia jak najwięcej zdjęć kubków - jeden typ', 'LOW', 'IN PROGRESS', 'MSDNA-001', 'Zdjęcia obiektu - Kubek', 1, 1, 5, 4, 0),
(8, '01-06-2020', '[Materiał poglądowy] - Załączyć do tego zgłoszenia jak najwięcej zdjęć klawiatury RGB', 'MED', 'BACKLOG', 'MSDNA-002', 'Zdjęcia obiektu - Klawiatura', 1, 1, 5, 4, 1),
(9, '01-06-2020', '[Materiał poglądowy] - Załączyć do tego zgłoszenia jak najwięcej zdjęć P-51', 'LOW', 'DONE', 'MSDNA-003', 'Zdjęcia obiektu - P-51 Mustang', 1, 1, 5, 4, 2),
(10, '01-06-2020', '[Materiał poglądowy] - Wykonanie kompletnego skanu dowolnego pojazdu i dołączenie go do tego zgłoszenia. Będzie stanowić wdrożenie w proces skanowania 3D', 'IMPROV', 'IN PROGRESS', 'MSDNA-004', 'Skan 3D - wdrożenie', 1, 1, 5, 4, 3),
(11, '01-06-2020', 'Klient wymaga nowej tekstury leśnej dla modelu M1A1', 'LOW', 'BACKLOG', 'MSDNA-005', 'Retekstura - M1A1 nowa tekstura leśna', 1, 1, 6, 5, 0),
(12, '01-06-2020', 'Klient domaga się nowego modelu pojazdu. P-51 Mustang. Materiały poglądowe dostarczone w innym zgłoszeniu', 'HIGH', 'IN PROGRESS', 'MSDNA-007', 'Nowy model - Samolot P-51', 1, 1, 6, 5, 1),
(13, '01-06-2020', 'Wymagana nowa wersja modelu postaci - Crusher', 'MED', 'BACKLOG', 'MSDNA-008', 'Nowa wersja modelu - Crusher Character', 1, 1, 6, 5, 2),
(14, '01-06-2020', 'Nowa mapa - QuinteLand. Zparojektuj i wykonaj mapę z wykorzystaniem narzędzia World Maker', 'HIGH', 'TESTING', 'MSDNA-009', 'QuinteLand - nowa mapa', 1, 1, 7, 7, 0),
(15, '01-06-2020', 'Nowe animacje dla postaci: Crusher. Animacje na podstawie mocapu', 'HIGH', 'IN PROGRESS', 'MSDNA-010', 'Crusher - nowe animacje', 1, 1, 7, 7, 1),
(16, '01-06-2020', 'Patch 0.20.2 Hotfix', 'HIGH', 'DONE', 'MSDNA-011', 'Update 0.20.2 Hotfix', 1, 1, 7, 8, 0),
(17, '01-06-2020', 'Patch 0.20.1', 'HIGH', 'DONE', 'MSDNA-012', 'Update 0.20.1', 1, 1, 6, 8, 1);


ALTER SEQUENCE public.tasks_id_seq RESTART WITH 18;